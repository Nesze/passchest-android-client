package hu.elte.passchest.utils;

import androidx.room.TypeConverter;
import hu.elte.passchest.data.model.api.entrydatabase.Status;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

public class Converters {

	@TypeConverter
	public static Date timestampToDate(Long value) {
		return value == null ? null : new Date(value);
	}

	@TypeConverter
	public static Long dateToTimestamp(Date date) {
		return date == null ? null : date.getTime();
	}
	
	@TypeConverter
	public static String charSequenceToString(CharSequence value) {
		return value.toString();
	}
	
	@TypeConverter
	public static Date stringToDate(String value) {
		try {
			DateFormat dateFormat = new SimpleDateFormat(AppConstants.TIMESTAMP_FORMAT, Locale.getDefault());
			return dateFormat.parse(value);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@TypeConverter
	public static String dateToString(Date date) {
		DateFormat dateFormat = new SimpleDateFormat(AppConstants.TIMESTAMP_FORMAT, Locale.getDefault());
		return dateFormat.format(date);
	}
	
	@TypeConverter
	public static String uuidToString(UUID uuid) {
		return uuid == null ? null : uuid.toString();
	}
	
	@TypeConverter
	public static UUID stringToUUID(String value) {
		return value == null ? null : UUID.fromString(value);
	}
	
	@TypeConverter
	public static String statusToString(Status status) { return status.toString(); }
	
	@TypeConverter
	public static Status stringToStatus(String value) { return Status.valueOf(value); }
}
