package hu.elte.passchest.ui.settings.editemail;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import androidx.lifecycle.ViewModelProviders;
import hu.elte.passchest.BR;
import hu.elte.passchest.R;
import hu.elte.passchest.databinding.ActivityEditEmailBinding;
import hu.elte.passchest.ui.base.BaseActivity;
import hu.elte.passchest.viewmodel.ViewModelProviderFactory;

import javax.inject.Inject;

public class EditEmailActivity extends BaseActivity<ActivityEditEmailBinding, EditEmailViewModel> implements EditEmailNavigator {
	
	@Inject
	ViewModelProviderFactory factory;
	
	private EditEmailViewModel mViewModel;
	private ActivityEditEmailBinding mBinding;
	
	@Override
	public int getBindingVariable() {
		return BR.viewModel;
	}
	
	@Override
	public int getLayoutId() {
		return R.layout.activity_edit_email;
	}
	
	@Override
	public EditEmailViewModel getViewModel() {
		mViewModel = ViewModelProviders.of(this, factory).get(EditEmailViewModel.class);
		return mViewModel;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mViewModel.setNavigator(this);
		mBinding = getViewDataBinding();
		
		mViewModel.getEmail().observe(this, email -> mBinding.editEmailEditTextEmail.setText(email));
	}
	
	public void onSubmitButtonClicked(View view) {
		CharSequence email = mBinding.editEmailEditTextEmail.getText();
		if (validateEmail(email)) {
			if (!mViewModel.getDataManager().getOfflineMode()) {
				mViewModel.setEmail(email);
				mViewModel.changeEmail();
			} else {
				Toast.makeText(this, getString(R.string.error_offline_mode), Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	private boolean validateEmail(CharSequence email) {
		boolean valid = true;
		if (mViewModel.isInputEmpty(email)) {
			valid = false;
			mBinding.editEmailEditTextEmailContainer.setError(getString(R.string.error_input_empty));
		} else {
			mBinding.editEmailEditTextEmailContainer.setError(null);
		}
		if (!mViewModel.isEmailValid(email)) {
			valid = false;
			mBinding.editEmailEditTextEmailContainer.setError(getString(R.string.error_input_invalid));
		} else {
			mBinding.editEmailEditTextEmailContainer.setError(null);
		}
		return valid;
	}
	
	@Override
	public void onActionComplete() {
		Toast.makeText(this, getString(R.string.success_edit_email), Toast.LENGTH_SHORT).show();
		closeActivity();
	}
	
	@Override
	public void handleError(Throwable throwable) {
		mBinding.editEmailEditTextEmailContainer.setError(getString(R.string.error_input_invalid));
	}
	
	@Override
	public void closeActivity() {
		finish();
		playClosingAnimation();
	}
	
	private void playClosingAnimation() {
		overridePendingTransition(R.anim.fade_in, R.anim.slide_out);
	}
}
