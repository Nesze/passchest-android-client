package hu.elte.passchest.ui.addresseditor;

import android.text.TextUtils;
import androidx.lifecycle.MutableLiveData;
import hu.elte.passchest.data.DataManager;
import hu.elte.passchest.ui.base.BaseViewModel;
import hu.elte.passchest.utils.rx.SchedulerProvider;

import java.net.MalformedURLException;
import java.net.URL;

public class AddressEditorViewModel extends BaseViewModel<AddressEditorNavigator> {
	
	private MutableLiveData<CharSequence> mFullAddress;
	
	private CharSequence mHost;
	private Integer mPort;
	private CharSequence mResource;
	
	public AddressEditorViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
		super(dataManager, schedulerProvider);
	}
	
	public CharSequence getHost() {
		checkDataIsLoaded();
		return mHost;
	}
	
	public Integer getPort() {
		checkDataIsLoaded();
		return mPort;
	}
	
	public CharSequence getResource() {
		checkDataIsLoaded();
		return mResource;
	}
	
	private void checkDataIsLoaded() {
		if (mFullAddress == null) {
			mFullAddress = new MutableLiveData<>();
			loadFullAddress();
		}
	}
	
	private void loadFullAddress() {
		mFullAddress.setValue(getDataManager().getServerAddress());
		try {
			URL url = new URL(mFullAddress.getValue().toString());
			mHost = url.getHost();
			mPort = url.getPort();
			mResource = url.getPath();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}
	
	public void setHost(CharSequence address) {
		mHost = address;
	}
	
	public void setPort(Integer port) {
		mPort = port;
	}
	
	public void setResource(CharSequence resource) {
		mResource = resource;
	}
	
	public void changeAddress() {
		CharSequence address = "https://" + mHost + ":" + mPort + mResource;
		getDataManager().setServerAddress(address);
		getNavigator().onActionComplete();
	}
	
	public boolean isInputEmpty(CharSequence input) {
		return TextUtils.isEmpty(input);
	}
	
	public boolean isHostValid(CharSequence email) {
		return email.toString().matches("^[a-z0-9-.]+$");
	}
	
	public boolean isValidPort(CharSequence port) {
		return port.toString().matches("^[0-9]{1,5}$");
	}
	
	public boolean isValidResource(CharSequence resource) {
		return resource.toString().matches("^/.*");
	}
}
