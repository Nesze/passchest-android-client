package hu.elte.passchest.ui.settings.editemail;

import android.text.TextUtils;
import android.util.Patterns;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import hu.elte.passchest.data.DataManager;
import hu.elte.passchest.data.model.api.user.UserRequest;
import hu.elte.passchest.ui.base.BaseViewModel;
import hu.elte.passchest.utils.rx.SchedulerProvider;

public class EditEmailViewModel extends BaseViewModel<EditEmailNavigator> {
	
	private MutableLiveData<CharSequence> mEmail;
	
	public EditEmailViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
		super(dataManager, schedulerProvider);
	}
	
	public LiveData<CharSequence> getEmail() {
		if (mEmail == null) {
			mEmail = new MutableLiveData<>();
			loadEmail();
		}
		return mEmail;
	}
	
	private void loadEmail() {
		mEmail.setValue(getDataManager().getEmail());
	}
	
	public void setEmail(CharSequence email) {
		mEmail.setValue(email);
	}
	
	public void changeEmail() {
		setIsLoading(true);
		getCompositeDisposable()
				.add(getDataManager()
						.doEditEmailApiCall(new UserRequest(mEmail.getValue().toString(), null, null))
						.subscribeOn(getSchedulerProvider().io())
						.observeOn(getSchedulerProvider().ui())
						.subscribe(userValidationResponse -> {
							setIsLoading(false);
							getDataManager().setEmail(mEmail.getValue());
							getNavigator().onActionComplete();
						}, throwable -> {
							setIsLoading(false);
							getNavigator().handleError(throwable);
						})
				);
	}
	
	public boolean isInputEmpty(CharSequence input) {
		return TextUtils.isEmpty(input);
	}
	
	public boolean isEmailValid(CharSequence email) {
		return Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}
}
