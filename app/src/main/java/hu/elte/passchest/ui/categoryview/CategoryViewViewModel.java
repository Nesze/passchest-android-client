package hu.elte.passchest.ui.categoryview;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import hu.elte.passchest.data.DataManager;
import hu.elte.passchest.data.model.entity.Category;
import hu.elte.passchest.ui.base.BaseViewModel;
import hu.elte.passchest.utils.rx.SchedulerProvider;
import io.reactivex.Single;

import java.util.Comparator;
import java.util.List;
import java.util.UUID;

public class CategoryViewViewModel extends BaseViewModel<CategoryViewNavigator> {
	
	private MutableLiveData<List<Category>> mCategories;
	
	public CategoryViewViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
		super(dataManager, schedulerProvider);
	}
	
	public LiveData<List<Category>> getCategories() {
		if (mCategories == null) {
			mCategories = new MutableLiveData<>();
			loadCategories();
		}
		return mCategories;
	}
	
	public void updateCategoriesFromCloud() {
		setIsLoading(true);
		getCompositeDisposable()
				.add(getDataManager()
						.doDownloadEntryDbApiCall()
						.flatMapCompletable(response -> getDataManager().mergeFromEntryDatabase(response))
						.andThen(getDataManager().getAllCategories())
						.flatMap(this::sortAlphabeticallyAscending)
						.subscribeOn(getSchedulerProvider().io())
						.observeOn(getSchedulerProvider().ui())
						.subscribe(response -> {
							setIsLoading(false);
							getNavigator().onUpdateCompleted();
							mCategories.setValue(response);
						}, throwable -> {
							setIsLoading(false);
							getNavigator().handleError(throwable);
						})
				);
	}
	
	public void updateCategoriesFromLocalDb() {
		setIsLoading(true);
		getCompositeDisposable()
				.add(getDataManager()
						.getAllCategories()
						.flatMap(this::sortAlphabeticallyAscending)
						.subscribeOn(getSchedulerProvider().io())
						.observeOn(getSchedulerProvider().ui())
						.subscribe(response -> {
							setIsLoading(false);
							mCategories.setValue(response);
						}, throwable -> {
							setIsLoading(false);
							getNavigator().handleError(throwable);
						})
				);
	}
	
	public Boolean getOfflineMode() {
		return getDataManager().getOfflineMode();
	}
	
	private void loadCategories() {
		if (!getOfflineMode()) {
			updateCategoriesFromCloud();
		} else {
			updateCategoriesFromLocalDb();
		}
	}
	
	public void deleteCategory(UUID uuid) {
		setIsLoading(true);
		getCompositeDisposable()
				.add(getDataManager()
						.getCategoryByUuid(uuid)
						.flatMapCompletable(category -> getDataManager().markCategoryAsDeleted(category))
						.subscribeOn(getSchedulerProvider().io())
						.observeOn(getSchedulerProvider().ui())
						.subscribe(() -> {
							getNavigator().onCategoryListChanged();
							setIsLoading(false);
						}, throwable -> setIsLoading(false))
				);
	}
	
	private Single<List<Category>> sortAlphabeticallyAscending(List<Category> categories) {
		return Single.create(emitter -> {
			categories.sort(Comparator.comparing(e -> e.name));
			emitter.onSuccess(categories);
		});
	}
	
}
