package hu.elte.passchest.ui.entryview;

import java.util.UUID;

public interface EntryViewNavigator {

    void handleError(Throwable throwable);
    
    void editEntry(UUID uuid);

    void createEntry();
    
    void deleteEntry(UUID uuid);
    
    void onEntryListChanged();
    
    void onUpdateCompleted();

}
