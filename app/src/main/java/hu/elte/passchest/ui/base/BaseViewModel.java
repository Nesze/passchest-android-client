package hu.elte.passchest.ui.base;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.ViewModel;
import hu.elte.passchest.data.DataManager;
import hu.elte.passchest.utils.rx.SchedulerProvider;
import io.reactivex.disposables.CompositeDisposable;

import java.lang.ref.WeakReference;

public class BaseViewModel<N> extends ViewModel {
	
	private final DataManager mDataManager;
	
	private final SchedulerProvider mSchedulerProvider;
	
	private CompositeDisposable mCompositeDisposable;
	
	private WeakReference<N> mNavigator;
	
	private final ObservableBoolean mIsLoading;
	
	public BaseViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
		this.mDataManager = dataManager;
		this.mSchedulerProvider = schedulerProvider;
		this.mCompositeDisposable = new CompositeDisposable();
		mIsLoading = new ObservableBoolean();
	}
	
	@Override
	protected void onCleared() {
		mCompositeDisposable.dispose();
		super.onCleared();
	}
	
	public CompositeDisposable getCompositeDisposable() {
		return mCompositeDisposable;
	}
	
	public DataManager getDataManager() {
		return mDataManager;
	}
	
	public SchedulerProvider getSchedulerProvider() {
		return mSchedulerProvider;
	}
	
	public N getNavigator() {
		return mNavigator.get();
	}
	
	public void setNavigator(N navigator) {
		this.mNavigator = new WeakReference<>(navigator);
	}
	
	public ObservableBoolean getIsLoading() {
		return mIsLoading;
	}
	
	public void setIsLoading(boolean isLoading) {
		mIsLoading.set(isLoading);
	}
	
}
