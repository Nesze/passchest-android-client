package hu.elte.passchest.ui.main;

import hu.elte.passchest.data.DataManager;
import hu.elte.passchest.ui.base.BaseViewModel;
import hu.elte.passchest.utils.rx.SchedulerProvider;

import javax.inject.Inject;

public class MainViewModel extends BaseViewModel<MainNavigator> {
	
	@Inject
	public MainViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
		super(dataManager, schedulerProvider);
	}
	
	public Boolean getOfflineMode() {
		return getDataManager().getOfflineMode();
	}
	
	public void syncDatabase() {
		setIsLoading(true);
		getCompositeDisposable().add(getDataManager()
				.getDatabaseForExport()
				.flatMap(entryDatabase -> getDataManager()
						.doUploadEntryDbApiCall(entryDatabase))
				.subscribeOn(getSchedulerProvider().io())
				.observeOn(getSchedulerProvider().ui())
				.subscribe(
						userValidationResponse -> {
							getNavigator().onSyncCompleted();
							setIsLoading(false);
						},
						throwable -> {
							getNavigator().handleError(throwable);
							setIsLoading(false);
						})
		);
	}
	
	public Boolean getIsAutoSyncEnabled() {
		return getDataManager().getIsAutoSyncEnabled();
	}
	
}
