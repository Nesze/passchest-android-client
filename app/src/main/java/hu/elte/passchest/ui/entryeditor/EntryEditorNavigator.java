package hu.elte.passchest.ui.entryeditor;

public interface EntryEditorNavigator {

    void saveEntry();
    
    void onSaveCompleted();
    
    void closeActivity();

}
