package hu.elte.passchest.ui.categoryview;

import java.util.UUID;

public interface CategoryViewNavigator {
	
	void handleError(Throwable throwable);
	
	void editCategory(UUID uuid);
	
	void createCategory();
	
	void deleteCategory(UUID uuid);
	
	void onCategoryListChanged();
	
	void onUpdateCompleted();
	
}
