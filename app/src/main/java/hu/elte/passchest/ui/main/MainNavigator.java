package hu.elte.passchest.ui.main;

public interface MainNavigator {
	
	void onSyncCompleted();
	
	void handleError(Throwable throwable);
	
	void showEntryViewFragment();
	
	void showCategoryViewFragment();
	
	void syncDatabase();
	
	void openSettingsActivity();
	
	void logout();
	
}
