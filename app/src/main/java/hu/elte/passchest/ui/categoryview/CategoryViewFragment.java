package hu.elte.passchest.ui.categoryview;

import android.content.Intent;
import android.view.*;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import hu.elte.passchest.BR;
import hu.elte.passchest.R;
import hu.elte.passchest.data.model.entity.Category;
import hu.elte.passchest.databinding.FragmentCategoryViewBinding;
import hu.elte.passchest.ui.base.BaseFragment;
import hu.elte.passchest.ui.categoryeditor.CategoryEditorActivity;
import hu.elte.passchest.ui.main.MainActivity;
import hu.elte.passchest.viewmodel.ViewModelProviderFactory;

import javax.inject.Inject;
import java.util.UUID;

public class CategoryViewFragment extends BaseFragment<FragmentCategoryViewBinding, CategoryViewViewModel> implements CategoryViewNavigator, SwipeRefreshLayout.OnRefreshListener, ListView.OnItemClickListener {
	
	public static final String TAG = CategoryViewFragment.class.getSimpleName();
	
	@Inject
	ViewModelProviderFactory factory;
	
	private CategoryViewViewModel mViewModel;
	private FragmentCategoryViewBinding mBinding;
	
	public static CategoryViewFragment newInstance() {
		Bundle args = new Bundle();
		CategoryViewFragment fragment = new CategoryViewFragment();
		fragment.setArguments(args);
		return fragment;
	}
	
	@Override
	public int getBindingVariable() {
		return BR.viewModel;
	}
	
	@Override
	public int getLayoutId() {
		return R.layout.fragment_category_view;
	}
	
	@Override
	public CategoryViewViewModel getViewModel() {
		mViewModel = ViewModelProviders.of(this, factory).get(CategoryViewViewModel.class);
		return mViewModel;
	}
	
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mViewModel.setNavigator(this);
	}
	
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
	                         @Nullable Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		
		mBinding = getViewDataBinding();
		
		mViewModel.getCategories().observe(this, categories -> {
			ListView listView = mBinding.categoryView;
			listView.setOnItemClickListener(this);
			registerForContextMenu(listView);
			CategoryListAdapter adapter = new CategoryListAdapter(this.getContext(), R.layout.adapter_category_view_layout, categories);
			listView.setAdapter(adapter);
		});
		
		mBinding.swiperefresh.setOnRefreshListener(this);
		
		return view;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if (!mViewModel.getIsLoading().get()) {
			updateCategoriesFromLocalDb();
		}
	}
	
	@Override
	public void onRefresh() {
		if (mViewModel.getOfflineMode()) {
			updateCategoriesFromLocalDb();
		} else {
			updateCategoriesFromCloud();
		}
	}
	
	public void onRefreshButtonClicked() {
		if (mViewModel.getOfflineMode()) {
			updateCategoriesFromLocalDb();
		} else {
			updateCategoriesFromCloud();
		}
	}
	
	private void updateCategoriesFromLocalDb() {
		mViewModel.updateCategoriesFromLocalDb();
	}
	
	private void updateCategoriesFromCloud() {
		mViewModel.updateCategoriesFromCloud();
	}
	
	@Override
	public void handleError(Throwable throwable) {
	
	}
	
	@Override
	public void editCategory(UUID uuid) {
		Intent intent = CategoryEditorActivity.newIntent(this.getContext());
		intent.putExtra(CategoryEditorActivity.KEY_ID, uuid);
		startActivity(intent);
		playNewActivityTransition();
	}
	
	@Override
	public void createCategory() {
		startActivity(CategoryEditorActivity.newIntent(this.getContext()));
		playNewActivityTransition();
	}
	
	@Override
	public void deleteCategory(UUID uuid) {
		mViewModel.deleteCategory(uuid);
	}
	
	@Override
	public void onCategoryListChanged() {
		updateCategoriesFromLocalDb();
	}
	
	@Override
	public void onUpdateCompleted() {
		Toast.makeText(getActivity(), getString(R.string.success_refresh), Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		editCategory(((Category) mBinding.categoryView.getItemAtPosition(position)).uuid);
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if (v.getId() == mBinding.categoryView.getId()) {
			MenuInflater inflater = getActivity().getMenuInflater();
			inflater.inflate(R.menu.context_category, menu);
		}
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		Category selectedCategory = (Category) mBinding.categoryView.getAdapter().getItem(info.position);
		switch (item.getItemId()) {
			case R.id.context_entry_edit:
				editCategory(selectedCategory.uuid);
				return true;
			case R.id.context_entry_delete:
				deleteCategory(selectedCategory.uuid);
				return true;
			default:
				return super.onContextItemSelected(item);
		}
	}
	
	private void playNewActivityTransition() {
		if (getActivity() != null) {
			((MainActivity) getActivity()).playNewActivityTransition();
		}
	}
	
}
