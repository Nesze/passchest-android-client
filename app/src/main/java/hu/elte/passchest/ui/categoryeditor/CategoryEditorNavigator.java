package hu.elte.passchest.ui.categoryeditor;

public interface CategoryEditorNavigator {

    void saveCategory();

    void onSaveCompleted();
    
    void closeActivity();

}
