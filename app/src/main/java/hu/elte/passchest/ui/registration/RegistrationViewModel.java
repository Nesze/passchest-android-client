package hu.elte.passchest.ui.registration;

import android.text.TextUtils;
import android.util.Patterns;
import androidx.lifecycle.MutableLiveData;
import hu.elte.passchest.data.DataManager;
import hu.elte.passchest.data.model.api.user.UserRequest;
import hu.elte.passchest.ui.base.BaseViewModel;
import hu.elte.passchest.utils.rx.SchedulerProvider;

import javax.inject.Inject;

public class RegistrationViewModel extends BaseViewModel<RegistrationNavigator> {
    
    private MutableLiveData<CharSequence> mEmail;
    private MutableLiveData<CharSequence> mUsername;
    private MutableLiveData<CharSequence> mPassword;
    
    @Inject
    public RegistrationViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        mEmail = new MutableLiveData<>();
        mUsername = new MutableLiveData<>();
        mPassword = new MutableLiveData<>();
    }
    
    // Cache
    
    public void setEmail(CharSequence email) {
        mEmail.setValue(email);
    }
    
    public void setUsername(CharSequence username) {
        mUsername.setValue(username);
    }
    
    public void setPassword(CharSequence password) {
        mPassword.setValue(password);
    }
    
    // Authentication
    
    public void register() {
        setIsLoading(true);
        getCompositeDisposable()
                .add(getDataManager()
                        .doUserRegistrationApiCall(new UserRequest(mEmail.getValue().toString(), mUsername.getValue().toString(), mPassword.getValue().toString()))
                        .subscribeOn(getSchedulerProvider().io())
                        .observeOn(getSchedulerProvider().ui())
                        .subscribe(response -> {
                            setIsLoading(false);
                            getNavigator().onRegistrationComplete();
                        }, throwable -> {
                            setIsLoading(false);
                            getNavigator().handleError(throwable);
                        }
                )
        );
    }
    
    public boolean isInputEmpty(CharSequence input) {
        return TextUtils.isEmpty(input);
    }
    
    public boolean isEmailValid(CharSequence email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    
    public boolean isUsernameValid(CharSequence username) {
        return username.length() >= 3 && username.length() <= 32;
    }
    
    public boolean isPasswordValid(CharSequence password) {
        return password.length() >= 8;
    }
    
    public boolean isMatchingPassword(CharSequence password, CharSequence password2) {
        return password.toString().equals(password2.toString());
    }

}
