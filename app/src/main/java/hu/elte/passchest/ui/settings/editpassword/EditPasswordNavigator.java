package hu.elte.passchest.ui.settings.editpassword;

public interface EditPasswordNavigator {
	
	void onActionComplete();
	
	void handleError(Throwable throwable);
	
	void closeActivity();
	
}
