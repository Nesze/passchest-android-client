package hu.elte.passchest.ui.auth;

public interface AuthNavigator {
	
	void handleError(Throwable throwable);
	
	void login();
	
	void onAuthorizationComplete();
	
	void openMainActivity();
	
	void openRegistrationActivity();
	
	void openAddressEditorActivity();
	
	void hideKeyboard();
	
}
