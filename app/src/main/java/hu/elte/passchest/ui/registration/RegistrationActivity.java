package hu.elte.passchest.ui.registration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;
import androidx.lifecycle.ViewModelProviders;
import com.androidnetworking.error.ANError;
import hu.elte.passchest.BR;
import hu.elte.passchest.R;
import hu.elte.passchest.data.model.api.user.UserValidationResponse;
import hu.elte.passchest.databinding.ActivityRegistrationBinding;
import hu.elte.passchest.ui.base.BaseActivity;
import hu.elte.passchest.viewmodel.ViewModelProviderFactory;

import javax.inject.Inject;

public class RegistrationActivity extends BaseActivity<ActivityRegistrationBinding, RegistrationViewModel> implements RegistrationNavigator {

	@Inject
	ViewModelProviderFactory factory;

	private RegistrationViewModel mViewModel;
	private ActivityRegistrationBinding mBinding;
	
	public static Intent newIntent(Context context) {
		return new Intent(context, RegistrationActivity.class);
	}
	
	@Override
	public int getBindingVariable() {
		return BR.viewModel;
	}
	
	@Override
	public int getLayoutId() {
		return R.layout.activity_registration;
	}
	
	@Override
	public RegistrationViewModel getViewModel() {
		mViewModel = ViewModelProviders.of(this, factory).get(RegistrationViewModel.class);
		return mViewModel;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mViewModel.setNavigator(this);
		mBinding = getViewDataBinding();
		
		hideKeyboard();
		
	}
	
	public void onSubmitButtonClicked(View view) {
		CharSequence email = mBinding.regEditTextEmail.getText();
		CharSequence username = mBinding.regEditTextUsername.getText();
		CharSequence password = mBinding.regEditTextPassword.getText();
		CharSequence password2 = mBinding.regEditTextPasswordAgain.getText();
		
		if (validateEmail(email) & validateUsername(username) & validatePassword(password, password2)) {
			mViewModel.setEmail(email);
			mViewModel.setUsername(username);
			mViewModel.setPassword(password);
			mViewModel.register();
		}
		
	}
	
	private boolean validateEmail(CharSequence email) {
		boolean valid = true;
		if (mViewModel.isInputEmpty(email)) {
			valid = false;
			mBinding.regEditTextEmailContainer.setError(getString(R.string.error_input_empty));
		} else {
			mBinding.regEditTextEmailContainer.setError(null);
		}
		if (!mViewModel.isEmailValid(email)) {
			valid = false;
			mBinding.regEditTextEmailContainer.setError(getString(R.string.error_input_invalid));
		} else {
			mBinding.regEditTextEmailContainer.setError(null);
		}
		return valid;
	}
	
	private boolean validateUsername(CharSequence username) {
		boolean valid = true;
		if (mViewModel.isInputEmpty(username)) {
			valid = false;
			mBinding.regEditTextUsernameContainer.setError(getString(R.string.error_input_empty));
		} else {
			mBinding.regEditTextUsernameContainer.setError(null);
		}
		if (!mViewModel.isUsernameValid(username)) {
			valid = false;
			mBinding.regEditTextUsernameContainer.setError(getString(R.string.error_input_username_length));
		} else {
			mBinding.regEditTextUsernameContainer.setError(null);
		}
		return valid;
	}
	
	private boolean validatePassword(CharSequence password, CharSequence password2) {
		boolean valid = true;
		if (mViewModel.isInputEmpty(password)) {
			valid = false;
			mBinding.regEditTextPasswordContainer.setError(getString(R.string.error_input_empty));
		} else {
			mBinding.regEditTextPasswordContainer.setError(null);
		}
		if (!mViewModel.isPasswordValid(password)) {
			valid = false;
			mBinding.regEditTextPasswordContainer.setError(getString(R.string.error_input_password_length));
		} else {
			mBinding.regEditTextPasswordContainer.setError(null);
		}
		if (!mViewModel.isMatchingPassword(password, password2)) {
			valid = false;
			mBinding.regEditTextPasswordAgainContainer.setError(getString(R.string.error_input_password_mismatch));
		} else {
			mBinding.regEditTextPasswordAgainContainer.setError(null);
		}
		return valid;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			playClosingAnimation();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		playClosingAnimation();
	}

	private void playClosingAnimation() {
		overridePendingTransition(R.anim.fade_in, R.anim.slide_out);
	}
	
	@Override
	public void handleError(Throwable throwable) {
		if (throwable instanceof ANError) {
			if (((ANError) throwable).getErrorCode() == 0) {
				if (((ANError) throwable).getErrorDetail().equals("connectionError")) {
					Toast.makeText(this, getString(R.string.error_network_timeout), Toast.LENGTH_SHORT).show();
				} else {
					throw new UnknownError();
				}
			} else {
				UserValidationResponse response = ((ANError) throwable).getErrorAsObject(UserValidationResponse.class);
				if (response.getUsernameStatus() == UserValidationResponse.Status.OCCUPIED) {
					mBinding.regEditTextUsernameContainer.setError(getString(R.string.error_input_username_occupied));
				} else if (response.getUsernameStatus() == UserValidationResponse.Status.INVALID) {
					mBinding.regEditTextUsernameContainer.setError(getString(R.string.error_input_username_length));
				}
				if (response.getEmailStatus() == UserValidationResponse.Status.INVALID) {
					mBinding.regEditTextEmailContainer.setError(getString(R.string.error_input_invalid));
				}
				if (response.getPasswordStatus() == UserValidationResponse.Status.INVALID) {
					mBinding.regEditTextPasswordContainer.setError(getString(R.string.error_input_password_length));
				}
				if (((ANError) throwable).getErrorCode() != 400 && ((ANError) throwable).getErrorCode() != 409) {
					Toast.makeText(this, getString(R.string.error_unknown), Toast.LENGTH_SHORT).show();
				}
			}
		}
	}
	
	@Override
	public void onRegistrationComplete() {
		closeActivity();
	}
	
	@Override
	public void closeActivity() {
		this.finish();
		playClosingAnimation();
	}
	
	@Override
	public void hideKeyboard() {
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
	}
}
