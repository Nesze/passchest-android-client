package hu.elte.passchest.ui.auth;

import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.androidnetworking.error.ANError;
import hu.elte.passchest.data.DataManager;
import hu.elte.passchest.ui.base.BaseViewModel;
import hu.elte.passchest.utils.rx.SchedulerProvider;
import org.mindrot.jbcrypt.BCrypt;

import javax.inject.Inject;

public class AuthViewModel extends BaseViewModel<AuthNavigator> {
    
    private MutableLiveData<CharSequence> mUsername;
    private MutableLiveData<CharSequence> mPassword;
    private Boolean mIsOffline;

    @Inject
    public AuthViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        mUsername = new MutableLiveData<>();
        mPassword = new MutableLiveData<>();
    }

    // Cache
    
    public LiveData<CharSequence> getUsername() {
        if (mUsername.getValue() == null) {
            loadUsername();
        }
        return mUsername;
    }
    
    public Boolean getIsUsernameAutofillEnabled() {
        return getDataManager().getIsUsernameAutofillEnabled();
    }

    public void setUsername(CharSequence username) {
        this.mUsername.setValue(username);
    }
    
    public void setPassword(CharSequence password) {
    	this.mPassword.setValue(password);
    }

    private void loadUsername() {
        setUsername(getDataManager().getUsername());
    }
    
    public void setIsOffline(Boolean value) {
        mIsOffline = value;
    }
    
    // Authentication

    public void login(CharSequence username, CharSequence password) {
        setIsLoading(true);
        if (mIsOffline) {
            if (isUsernameMatchingLocal(username) && getDataManager().isPasswordValid(password)) {
                setIsLoading(false);
                getDataManager().setOfflineMode(true);
                getNavigator().onAuthorizationComplete();
            } else {
                setIsLoading(false);
                ANError error = new ANError();
                error.setErrorCode(401);
                getNavigator().handleError(error);
            }
        } else {
            getDataManager().setHeaderDetails(username, password);
            getCompositeDisposable()
                    .add(getDataManager()
                            .doUserDetailsApiCall()
                            .subscribeOn(getSchedulerProvider().io())
                            .observeOn(getSchedulerProvider().ui())
                            .subscribe(response -> {
                                        getDataManager().setUserDetails(response.getUsername(), response.getEmail(), response.getEntryDbDate());
                                        getDataManager().setPasswordHash(BCrypt.hashpw(password.toString(), BCrypt.gensalt()));
                                        getDataManager().setOfflineMode(false);
                                        setIsLoading(false);
                                        getNavigator().onAuthorizationComplete();
                                    }, throwable -> {
                                        setIsLoading(false);
                                        getNavigator().handleError(throwable);
                                    }
                            )
                    );
        }
    }
    
    private boolean isUsernameMatchingLocal(CharSequence username) {
        return getDataManager().getUsername() != null && getDataManager().getUsername().toString().equals(username.toString());
    }
    
    public boolean isUsernameValid(CharSequence username) {
        return !TextUtils.isEmpty(username);
    }
    
    public boolean isPasswordValid(CharSequence password) {
        return !TextUtils.isEmpty(password);
    }
    
    public void clearAllTables(Callback callback) {
        setIsLoading(true);
        getCompositeDisposable()
                .add(getDataManager()
                        .clearAllTables()
                        .subscribeOn(getSchedulerProvider().io())
                        .observeOn(getSchedulerProvider().ui())
                        .subscribe(() -> {
                            setIsLoading(false);
                            callback.onComplete();
                        }, throwable -> setIsLoading(false))
                );
    }
    
    public interface Callback {
        void onComplete();
    }
    
}
