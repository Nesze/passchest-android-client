package hu.elte.passchest.ui.settings.editpassword;

import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import hu.elte.passchest.data.DataManager;
import hu.elte.passchest.data.model.api.user.UserRequest;
import hu.elte.passchest.ui.base.BaseViewModel;
import hu.elte.passchest.utils.rx.SchedulerProvider;
import org.mindrot.jbcrypt.BCrypt;

public class EditPasswordViewModel extends BaseViewModel<EditPasswordNavigator> {
	
	private MutableLiveData<CharSequence> mCurrentPassword;
	
	public EditPasswordViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
		super(dataManager, schedulerProvider);
	}
	
	public LiveData<CharSequence> getCurrentPassword() {
		if (mCurrentPassword == null) {
			mCurrentPassword = new MutableLiveData<>();
			loadCurrentPassword();
		}
		return mCurrentPassword;
	}
	
	private void loadCurrentPassword() {
		mCurrentPassword.setValue(getDataManager().getPasswordHash());
	}
	
	public void changePassword(CharSequence newPassword) {
		setIsLoading(true);
		getCompositeDisposable()
				.add(getDataManager()
						.doEditPasswordApiCall(new UserRequest(null, null, newPassword.toString()))
						.subscribeOn(getSchedulerProvider().io())
						.observeOn(getSchedulerProvider().ui())
						.subscribe(userValidationResponse -> {
							setIsLoading(false);
							getDataManager().setHeaderDetails(getDataManager().getUsername(), newPassword);
							getDataManager().setPasswordHash(BCrypt.hashpw(newPassword.toString(), BCrypt.gensalt()));
							getNavigator().onActionComplete();
						}, throwable -> {
							setIsLoading(false);
							getNavigator().handleError(throwable);
						})
				);
	}
	
	public boolean isInputEmpty(CharSequence input) {
		return TextUtils.isEmpty(input);
	}
	
	public boolean isPasswordValid(CharSequence password) {
		return password.length() >= 8;
	}
	
	public boolean isMatchingPassword(CharSequence password, CharSequence password2) {
		return password.toString().equals(password2.toString());
	}
}
