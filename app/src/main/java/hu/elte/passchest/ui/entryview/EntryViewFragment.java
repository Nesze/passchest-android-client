package hu.elte.passchest.ui.entryview;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.view.*;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import hu.elte.passchest.BR;
import hu.elte.passchest.R;
import hu.elte.passchest.data.model.entity.Entry;
import hu.elte.passchest.databinding.FragmentEntryViewBinding;
import hu.elte.passchest.ui.base.BaseFragment;
import hu.elte.passchest.ui.entryeditor.EntryEditorActivity;
import hu.elte.passchest.ui.main.MainActivity;
import hu.elte.passchest.viewmodel.ViewModelProviderFactory;

import javax.inject.Inject;
import java.util.UUID;

public class EntryViewFragment extends BaseFragment<FragmentEntryViewBinding, EntryViewViewModel> implements EntryViewNavigator, SwipeRefreshLayout.OnRefreshListener, ListView.OnItemClickListener {
	
	public static final String TAG = EntryViewFragment.class.getSimpleName();
	
	@Inject
	ViewModelProviderFactory factory;
	
	private EntryViewViewModel mViewModel;
	private FragmentEntryViewBinding mBinding;
	
	public static EntryViewFragment newInstance() {
		Bundle args = new Bundle();
		EntryViewFragment fragment = new EntryViewFragment();
		fragment.setArguments(args);
		return fragment;
	}
	
	@Override
	public int getBindingVariable() {
		return BR.viewModel;
	}
	
	@Override
	public int getLayoutId() {
		return R.layout.fragment_entry_view;
	}
	
	@Override
	public EntryViewViewModel getViewModel() {
		mViewModel = ViewModelProviders.of(this, factory).get(EntryViewViewModel.class);
		return mViewModel;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mViewModel.setNavigator(this);
	}
	
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		
		mBinding = getViewDataBinding();
		
		mViewModel.getEntries().observe(this, entries -> {
			mViewModel.getCategories().observe(this, categories -> {
				ListView listView = mBinding.entryView;
				listView.setOnItemClickListener(this);
				registerForContextMenu(listView);
				EntryListAdapter adapter = new EntryListAdapter(this.getContext(), R.layout.adapter_entry_view_layout, mViewModel.getEntries().getValue(), categories);
				listView.setAdapter(adapter);
			});
		});
		
		mBinding.swiperefresh.setOnRefreshListener(this);
		
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		if (!mViewModel.getIsLoading().get()) {
			updateEntriesFromLocalDb();
		}
	}

	@Override
	public void onRefresh() {
		if (mViewModel.getOfflineMode()) {
			updateEntriesFromLocalDb();
		} else {
			updateEntriesFromCloud();
		}
	}
	
	public void onRefreshButtonClicked() {
		if (mViewModel.getOfflineMode()) {
			updateEntriesFromLocalDb();
		} else {
			updateEntriesFromCloud();
		}
	}

	private void updateEntriesFromLocalDb() {
		mViewModel.updateEntriesFromLocalDb();
	}

	private void updateEntriesFromCloud() {
		mViewModel.updateEntriesFromCloud();
	}
	
	@Override
	public void handleError(Throwable throwable) {
		Toast.makeText(getActivity(), getString(R.string.error_server_error), Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public void editEntry(UUID uuid) {
		Intent intent = EntryEditorActivity.newIntent(this.getContext());
		intent.putExtra(EntryEditorActivity.KEY_ID, uuid);
		startActivity(intent);
		playNewActivityTransition();
	}

	@Override
	public void createEntry() {
		startActivity(EntryEditorActivity.newIntent(this.getContext()));
		playNewActivityTransition();
	}
	
	@Override
	public void deleteEntry(UUID uuid) {
		mViewModel.deleteEntry(uuid);
	}
	
	@Override
	public void onEntryListChanged() {
		updateEntriesFromLocalDb();
	}
	
	@Override
	public void onUpdateCompleted() {
		Toast.makeText(getActivity(), getString(R.string.success_refresh), Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		editEntry(((Entry) mBinding.entryView.getItemAtPosition(position)).uuid);
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if (v.getId() == mBinding.entryView.getId()) {
			MenuInflater inflater = getActivity().getMenuInflater();
			inflater.inflate(R.menu.context_entry, menu);
		}
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		Entry selectedEntry = (Entry) mBinding.entryView.getAdapter().getItem(info.position);
		switch (item.getItemId()) {
			case R.id.context_entry_edit:
				editEntry(selectedEntry.uuid);
				return true;
			case R.id.context_entry_delete:
				deleteEntry(selectedEntry.uuid);
				return true;
			case R.id.context_entry_copy_clipboard:
				ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
				ClipData clip = ClipData.newPlainText("Password", selectedEntry.password);
				clipboard.setPrimaryClip(clip);
				return true;
			default:
				return super.onContextItemSelected(item);
		}
	}
	
	private void playNewActivityTransition() {
		if (getActivity() != null) {
			((MainActivity) getActivity()).playNewActivityTransition();
		}
	}
}
