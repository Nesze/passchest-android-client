package hu.elte.passchest.ui.categoryeditor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import androidx.lifecycle.ViewModelProviders;
import hu.elte.passchest.BR;
import hu.elte.passchest.R;
import hu.elte.passchest.data.model.entity.Category;
import hu.elte.passchest.databinding.ActivityCategoryEditorBinding;
import hu.elte.passchest.ui.base.BaseActivity;
import hu.elte.passchest.viewmodel.ViewModelProviderFactory;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import java.util.Date;
import java.util.UUID;

public class CategoryEditorActivity extends BaseActivity<ActivityCategoryEditorBinding, CategoryEditorViewModel> implements CategoryEditorNavigator {
	
	public static final String KEY_ID = "KEY_ID";
	
	@Inject
	ViewModelProviderFactory factory;
	
	private CategoryEditorViewModel mViewModel;
	private ActivityCategoryEditorBinding mBinding;
	
	public static Intent newIntent(Context context) {
		return new Intent(context, CategoryEditorActivity.class);
	}
	
	@Override
	public int getBindingVariable() {
		return BR.viewModel;
	}
	
	@Override
	public int getLayoutId() {
		return R.layout.activity_category_editor;
	}
	
	@Override
	public CategoryEditorViewModel getViewModel() {
		mViewModel = ViewModelProviders.of(this, factory).get(CategoryEditorViewModel.class);
		return mViewModel;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mViewModel.setNavigator(this);
		mBinding = getViewDataBinding();
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mViewModel.getCategory((UUID) extras.get(KEY_ID)).observe(this, this::updateFields);
		} else {
			mViewModel.getCategory().observe(this, this::updateFields);
		}
	}

	private void updateFields(@NotNull Category category) {
		mBinding.categoryEditorEditTextName.setText(category.name);
	}

	public void onSaveButtonClicked(View view) {
		saveCategory();
	}

	@Override
	public void saveCategory() {
		CharSequence name = mBinding.categoryEditorEditTextName.getText();
		Date dateModified = new Date();

		if (validateName(name)) {
			mViewModel.updateCategory(name, dateModified);
			mViewModel.saveCategory();
		}
	}
	
	@Override
	public void onSaveCompleted() {
		Toast.makeText(this, getString(R.string.success_save_category), Toast.LENGTH_SHORT).show();
		closeActivity();
	}
	
	@Override
	public void closeActivity() {
		finish();
		playClosingAnimation();
	}

	private void playClosingAnimation() {
		overridePendingTransition(R.anim.fade_in, R.anim.slide_out);
	}

	private boolean validateName(CharSequence name) {
		boolean valid = true;
		if (TextUtils.isEmpty(name)) {
			valid = false;
			mBinding.categoryEditorEditTextNameContainer.setError(getString(R.string.error_input_empty));
		} else {
			mBinding.categoryEditorEditTextNameContainer.setError(null);
		}
		return valid;
	}

}
