package hu.elte.passchest.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.view.View;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import hu.elte.passchest.BR;
import hu.elte.passchest.R;
import hu.elte.passchest.databinding.ActivityMainBinding;
import hu.elte.passchest.ui.auth.AuthActivity;
import hu.elte.passchest.ui.base.BaseActivity;
import hu.elte.passchest.ui.base.BaseFragment;
import hu.elte.passchest.ui.categoryview.CategoryViewFragment;
import hu.elte.passchest.ui.entryview.EntryViewFragment;
import hu.elte.passchest.ui.settings.SettingsActivity;
import hu.elte.passchest.viewmodel.ViewModelProviderFactory;

import javax.inject.Inject;

public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel> implements MainNavigator, NavigationView.OnNavigationItemSelectedListener {
	
	@Inject
	ViewModelProviderFactory factory;
	
	private MainViewModel mViewModel;
	private ActivityMainBinding mBinding;
	
	private boolean mLoggedOut;
	
	public static Intent newIntent(Context context) {
		return new Intent(context, MainActivity.class);
	}
	
	@Override
	public int getBindingVariable() {
		return BR.viewModel;
	}
	
	@Override
	public int getLayoutId() {
		return R.layout.activity_main;
	}
	
	@Override
	public MainViewModel getViewModel() {
		mViewModel = ViewModelProviders.of(this, factory).get(MainViewModel.class);
		return mViewModel;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mViewModel.setNavigator(this);
		mBinding = getViewDataBinding();
		
		mLoggedOut = false;
		
		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		
		FloatingActionButton fab = findViewById(R.id.fab);
		fab.setOnClickListener(this::onFabClicked);
		
		DrawerLayout drawer = mBinding.drawerLayout;
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
				this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.addDrawerListener(toggle);
		toggle.syncState();
		
		NavigationView navigationView = mBinding.navView;
		navigationView.setNavigationItemSelectedListener(this);
		
		showEntryViewFragment();
	}
	
	@Override
	protected void onStop() {
		if (!mViewModel.getOfflineMode() && mViewModel.getIsAutoSyncEnabled()) {
			syncDatabase();
		}
		super.onStop();
	}
	
	private void onFabClicked(View view) {
		Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.main_fragment_container);
		if (fragment instanceof EntryViewFragment) {
			((EntryViewFragment) fragment).createEntry();
		} else if (fragment instanceof  CategoryViewFragment) {
			((CategoryViewFragment) fragment).createCategory();
		}
	}
	
	@Override
	public void onBackPressed() {
		DrawerLayout drawer = mBinding.drawerLayout;
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		} else {
			super.onBackPressed();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.actions_main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == R.id.action_refresh) {
			Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.main_fragment_container);
			if (fragment instanceof EntryViewFragment) {
				((EntryViewFragment) fragment).onRefreshButtonClicked();
			} else if (fragment instanceof  CategoryViewFragment) {
				((CategoryViewFragment) fragment).onRefreshButtonClicked();
			}
		} else if (id == R.id.action_sync) {
			if (mViewModel.getOfflineMode()) {
				Toast.makeText(this, getString(R.string.error_offline_mode), Toast.LENGTH_SHORT).show();
			} else {
				syncDatabase();
			}
		}

		return super.onOptionsItemSelected(item);
	}
	
	@SuppressWarnings("StatementWithEmptyBody")
	@Override
	public boolean onNavigationItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.nav_entry_view:
				showEntryViewFragment();
				break;
			case R.id.nav_category_view:
				showCategoryViewFragment();
				break;
			case R.id.nav_settings:
				openSettingsActivity();
				break;
			case R.id.nav_log_out:
				logout();
				break;
			default:
				throw new UnsupportedOperationException();
		}
		
		mBinding.drawerLayout.closeDrawer(GravityCompat.START);
		return true;
	}
	
	@Override
	public void onSyncCompleted() {
		Toast.makeText(this, getString(R.string.success_sync), Toast.LENGTH_SHORT).show();
		if (mLoggedOut) {
			finish();
		}
	}
	
	@Override
	public void handleError(Throwable throwable) {
		Toast.makeText(this, getString(R.string.error_server_error), Toast.LENGTH_SHORT).show();
	}
	
	private <T extends BaseFragment> void showFragment(Class<T> f, String tag) {
		if (getSupportFragmentManager().findFragmentByTag(tag) == null) {
			try {
				getSupportFragmentManager()
						.beginTransaction()
						.disallowAddToBackStack()
						.replace(R.id.main_fragment_container, f.newInstance(), tag)
						.commit();
			} catch (IllegalAccessException | InstantiationException e) {
				e.printStackTrace();
			}
		} else {
			Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
			getSupportFragmentManager()
					.beginTransaction()
					.disallowAddToBackStack()
					.replace(R.id.main_fragment_container, fragment, tag)
					.commit();
		}
	}
	
	@Override
	public void showEntryViewFragment() {
		setTitle(getString(R.string.title_fragment_entry_view));
		showFragment(EntryViewFragment.class, EntryViewFragment.TAG);
	}
	
	@Override
	public void showCategoryViewFragment() {
		setTitle(getString(R.string.title_fragment_category_view));
		showFragment(CategoryViewFragment.class, CategoryViewFragment.TAG);
	}
	
	@Override
	public void syncDatabase() {
		mViewModel.syncDatabase();
	}
	
	@Override
	public void openSettingsActivity() {
		startActivity(SettingsActivity.newIntent(this));
	}
	
	@Override
	public void logout() {
		startActivity(AuthActivity.newIntent(this));
		if (mViewModel.getOfflineMode()) {
			finish();
		} else {
			mLoggedOut = true;
		}
	}
	
	public void playNewActivityTransition() {
		overridePendingTransition(R.anim.slide_in, R.anim.fade_out);
	}
	
}
