package hu.elte.passchest.ui.registration;

public interface RegistrationNavigator {
	
	void handleError(Throwable throwable);
	
	void onRegistrationComplete();
	
	void closeActivity();
	
	void hideKeyboard();
	
}
