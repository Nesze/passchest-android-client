package hu.elte.passchest.ui.categoryview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import hu.elte.passchest.R;
import hu.elte.passchest.data.model.entity.Category;

import java.util.List;

public class CategoryListAdapter extends ArrayAdapter<Category> {
	
	public static final String TAG = "CategoryListAdapter";
	
	private Context mContext;
	private int mResource;
	
	static class ViewHolder {
		TextView name;
	}
	
	public CategoryListAdapter(Context context, int resource, List<Category> objects) {
		super(context, resource, objects);
		mContext = context;
		mResource = resource;
	}
	
	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(mContext);
			convertView = inflater.inflate(mResource, parent, false);
			holder = new ViewHolder();
			holder.name = convertView.findViewById(R.id.text_name);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.name.setText(getItem(position).name);
		
		return convertView;
	}
}
