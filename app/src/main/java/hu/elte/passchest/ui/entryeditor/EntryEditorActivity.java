package hu.elte.passchest.ui.entryeditor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;
import androidx.lifecycle.ViewModelProviders;
import hu.elte.passchest.BR;
import hu.elte.passchest.R;
import hu.elte.passchest.data.model.entity.Category;
import hu.elte.passchest.data.model.entity.Entry;
import hu.elte.passchest.databinding.ActivityEntryEditorBinding;
import hu.elte.passchest.ui.base.BaseActivity;
import hu.elte.passchest.viewmodel.ViewModelProviderFactory;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class EntryEditorActivity extends BaseActivity<ActivityEntryEditorBinding, EntryEditorViewModel> implements EntryEditorNavigator {
	
	public static final String KEY_ID = "KEY_ID";
	
	@Inject
	ViewModelProviderFactory factory;
	
	private EntryEditorViewModel mViewModel;
	private ActivityEntryEditorBinding mBinding;
	
	public static Intent newIntent(Context context) {
		return new Intent(context, EntryEditorActivity.class);
	}
	
	@Override
	public int getBindingVariable() {
		return BR.viewModel;
	}
	
	@Override
	public int getLayoutId() {
		return R.layout.activity_entry_editor;
	}
	
	@Override
	public EntryEditorViewModel getViewModel() {
		mViewModel = ViewModelProviders.of(this, factory).get(EntryEditorViewModel.class);
		return mViewModel;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mViewModel.setNavigator(this);
		mBinding = getViewDataBinding();

		Spinner categorySpinner = mBinding.entryEditorSpinnerCategory;
		List<Category> spinnerData = new ArrayList<>();
		CategorySpinnerAdapter adapter = new CategorySpinnerAdapter(this, R.layout.spinner_item, spinnerData);
		adapter.setDropDownViewResource(R.layout.spinner_item);
		categorySpinner.setAdapter(adapter);
		
		mViewModel.getCategories().observe(this, categories -> {
			Category none = new Category();
			none.uuid = null;
			none.name = "(none)";
			categories.add(0, none);
			CategorySpinnerAdapter categorySpinnerAdapter = (CategorySpinnerAdapter) mBinding.entryEditorSpinnerCategory.getAdapter();
			categorySpinnerAdapter.clear();
			categorySpinnerAdapter.addAll(categories);
			categorySpinnerAdapter.notifyDataSetChanged();
		});
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mViewModel.getEntry((UUID) extras.get(KEY_ID)).observe(this, this::updateFields);
		} else {
			mViewModel.getEntry().observe(this, this::updateFields);
		}
	}

	private void updateFields(@NotNull Entry entry) {
		mBinding.entryEditorEditTextDisplayName.setText(entry.displayName);
		mBinding.entryEditorEditTextUrl.setText(entry.url);
		mBinding.entryEditorEditTextUsername.setText(entry.username);
		mBinding.entryEditorEditTextPassword.setText(entry.password);
		if (entry.categoryUuid != null) {
			if (mViewModel.getCategories().getValue() == null) {
				mViewModel.getCategories().observe(this, categories -> updateSelectedCategory(categories, entry.categoryUuid));
			} else {
				updateSelectedCategory(mViewModel.getCategories().getValue(), entry.categoryUuid);
			}
		}
	}
	
	private void updateSelectedCategory(List<Category> categories, UUID categoryUuid) {
		CategorySpinnerAdapter categorySpinnerAdapter = (CategorySpinnerAdapter) mBinding.entryEditorSpinnerCategory.getAdapter();
		int pos = categorySpinnerAdapter.getPosition(mViewModel.findCategoryById(categories, categoryUuid));
		mBinding.entryEditorSpinnerCategory.setSelection(pos);
	}

	public void onSaveButtonClicked(View view) {
		saveEntry();
	}

	@Override
	public void saveEntry() {
		CharSequence displayName = mBinding.entryEditorEditTextDisplayName.getText();
		CharSequence url = mBinding.entryEditorEditTextUrl.getText();
		CharSequence username = mBinding.entryEditorEditTextUsername.getText();
		CharSequence password = mBinding.entryEditorEditTextPassword.getText();
		UUID categoryUuid = ((Category) mBinding.entryEditorSpinnerCategory.getSelectedItem()).uuid;
		Date dateModified = new Date();

		if (validateUrl(url) & validatePassword(password)) {
			mViewModel.updateEntry(displayName, url, username, password, categoryUuid, dateModified);
			mViewModel.saveEntry();
		}
	}
	
	@Override
	public void onSaveCompleted() {
		Toast.makeText(this, getString(R.string.success_save_entry), Toast.LENGTH_SHORT).show();
		closeActivity();
	}

	@Override
	public void closeActivity() {
		finish();
		playClosingAnimation();
	}

	private void playClosingAnimation() {
		overridePendingTransition(R.anim.fade_in, R.anim.slide_out);
	}

	private boolean validateUrl(CharSequence url) {
		boolean valid = true;
		if (TextUtils.isEmpty(url)) {
			valid = false;
			mBinding.entryEditorEditTextUrlContainer.setError(getString(R.string.error_input_empty));
		} else {
			mBinding.entryEditorEditTextUrlContainer.setError(null);
		}
		return valid;
	}

	private boolean validatePassword(CharSequence password) {
		boolean valid = true;
		if (TextUtils.isEmpty(password)) {
			valid = false;
			mBinding.entryEditorEditTextPasswordContainer.setError(getString(R.string.error_input_empty));
		} else {
			mBinding.entryEditorEditTextPasswordContainer.setError(null);
		}
		return valid;
	}
}
