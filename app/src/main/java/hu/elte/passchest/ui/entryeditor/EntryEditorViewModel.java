package hu.elte.passchest.ui.entryeditor;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import hu.elte.passchest.data.DataManager;
import hu.elte.passchest.data.model.api.entrydatabase.Status;
import hu.elte.passchest.data.model.entity.Category;
import hu.elte.passchest.data.model.entity.Entry;
import hu.elte.passchest.ui.base.BaseViewModel;
import hu.elte.passchest.utils.rx.SchedulerProvider;
import io.reactivex.Single;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class EntryEditorViewModel extends BaseViewModel<EntryEditorNavigator> {
	
	MutableLiveData<Entry> mEntry;
	MutableLiveData<List<Category>> mCategories;
	MutableLiveData<Category> mCategory;
	
	public EntryEditorViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
		super(dataManager, schedulerProvider);
	}
	
	public LiveData<List<Category>> getCategories() {
		if (mCategories == null) {
			mCategories = new MutableLiveData<>();
			loadCategories();
		}
		return mCategories;
	}
	
	private void loadCategories() {
		setIsLoading(true);
		getCompositeDisposable()
				.add(getDataManager()
						.getAllCategories()
						.flatMap(this::sortByName)
						.subscribeOn(getSchedulerProvider().io())
						.observeOn(getSchedulerProvider().ui())
						.subscribe(categories -> {
							mCategories.setValue(categories);
							setIsLoading(false);
						}, throwable -> setIsLoading(false))
				);
	}
	
	private Single<List<Category>> sortByName(List<Category> categories) {
		return Single.create(emitter -> {
			categories.sort(Comparator.comparing(c -> c.name));
			emitter.onSuccess(categories);
		});
	}
	
	public LiveData<Entry> getEntry() {
		return getEntry(null);
	}
	
	public LiveData<Entry> getEntry(UUID uuid) {
		if (mEntry == null) {
			mEntry = new MutableLiveData<>();
			if (uuid != null) {
				loadEntry(uuid);
			}
		}
		return mEntry;
	}

	public void updateEntry(CharSequence displayName, CharSequence url, CharSequence username, CharSequence password, UUID categoryUuid, Date dateModified) {
		if (mEntry.getValue() == null) {
			mEntry.setValue(new Entry());
		}
		Entry entry = mEntry.getValue();
		entry.status = Status.ACTIVE;
		entry.displayName = displayName.toString();
		entry.url = url.toString();
		entry.username = username.toString();
		entry.password = password.toString();
		entry.categoryUuid = categoryUuid;
		entry.dateModified = dateModified;
		mEntry.setValue(entry);
	}
	
	private void loadEntry(UUID uuid) {
		setIsLoading(true);
		getCompositeDisposable()
				.add(getDataManager()
						.getEntryByUuid(uuid)
						.subscribeOn(getSchedulerProvider().io())
						.observeOn(getSchedulerProvider().ui())
						.subscribe(entry -> {
							mEntry.setValue(entry);
							setIsLoading(false);
						}, throwable -> setIsLoading(false))
				);
	}
	
	public void saveEntry() {
		setIsLoading(true);
		getCompositeDisposable()
				.add(getDataManager()
						.insertEntry(mEntry.getValue())
						.subscribeOn(getSchedulerProvider().io())
						.observeOn(getSchedulerProvider().ui())
						.subscribe(() -> {
							getNavigator().onSaveCompleted();
							setIsLoading(false);
						})
				);
	}
	
	public Category findCategoryById(List<Category> categories, UUID uuid) {
		for (Category category : categories) {
			if (category.uuid == null && uuid == null || category.uuid != null && category.uuid.equals(uuid)) {
				return category;
			}
		}
		return null;
	}
	
}
