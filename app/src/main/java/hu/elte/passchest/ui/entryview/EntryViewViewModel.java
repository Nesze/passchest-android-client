package hu.elte.passchest.ui.entryview;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.EmptyResultSetException;
import hu.elte.passchest.data.DataManager;
import hu.elte.passchest.data.model.entity.Category;
import hu.elte.passchest.data.model.entity.Entry;
import hu.elte.passchest.ui.base.BaseViewModel;
import hu.elte.passchest.utils.rx.SchedulerProvider;
import io.reactivex.Single;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class EntryViewViewModel extends BaseViewModel<EntryViewNavigator> {
	
	MutableLiveData<List<Entry>> mEntries;
	MutableLiveData<List<Category>> mCategories;
	
	@Inject
	public EntryViewViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
		super(dataManager, schedulerProvider);
	}
	
	public LiveData<List<Entry>> getEntries() {
		if (mEntries == null) {
			mEntries = new MutableLiveData<>();
			loadEntries();
		}
		return mEntries;
	}
	
	public void updateEntriesFromCloud() {
		setIsLoading(true);
		getCompositeDisposable()
				.add(getDataManager()
						.doDownloadEntryDbApiCall()
						.flatMapCompletable(response -> getDataManager().mergeFromEntryDatabase(response))
						.andThen(getDataManager().getAllEntries())
						.flatMap(this::sortByCategoryName)
						.subscribeOn(getSchedulerProvider().io())
						.observeOn(getSchedulerProvider().ui())
						.subscribe(response -> {
							setIsLoading(false);
							getNavigator().onUpdateCompleted();
							mEntries.setValue(response);
						}, throwable -> {
							setIsLoading(false);
							getNavigator().handleError(throwable);
						})
				);
	}
	
	public void updateEntriesFromLocalDb() {
		setIsLoading(true);
		getCompositeDisposable()
				.add(getDataManager()
						.getAllEntries()
						.flatMap(this::sortByCategoryName)
						.subscribeOn(getSchedulerProvider().io())
						.observeOn(getSchedulerProvider().ui())
						.subscribe(response -> {
							setIsLoading(false);
							mEntries.setValue(response);
						}, throwable -> {
							setIsLoading(false);
							getNavigator().handleError(throwable);
						})
				);
	}
	
	public Boolean getOfflineMode() {
		return getDataManager().getOfflineMode();
	}
	
	private void loadEntries() {
		if (!getOfflineMode()) {
			updateEntriesFromCloud();
		} else {
			updateEntriesFromLocalDb();
		}
	}
	
	public void deleteEntry(UUID uuid) {
		setIsLoading(true);
		getCompositeDisposable()
				.add(getDataManager()
						.getEntryByUuid(uuid)
						.flatMapCompletable(entry -> getDataManager().markEntryAsDeleted(entry))
						.subscribeOn(getSchedulerProvider().io())
						.observeOn(getSchedulerProvider().ui())
						.subscribe(() -> {
							getNavigator().onEntryListChanged();
							setIsLoading(false);
						}, throwable -> setIsLoading(false))
				);
	}
	
	public LiveData<List<Category>> getCategories() {
		if (mCategories == null) {
			mCategories = new MutableLiveData<>();
			loadCategories();
		}
		return mCategories;
	}
	
	private void loadCategories() {
		setIsLoading(true);
		getCompositeDisposable()
				.add(getDataManager()
						.getAllCategories()
						.subscribeOn(getSchedulerProvider().io())
						.observeOn(getSchedulerProvider().ui())
						.subscribe(categories -> {
							setIsLoading(false);
							mCategories.setValue(categories);
						}, throwable -> setIsLoading(false))
		);
	}
	
	private Single<List<Entry>> sortByCategoryName(List<Entry> entries) {
		return Single.create(emitter -> {
			List<Category> results;
			try {
				results = getDataManager().getAllCategories().blockingGet();
			} catch (EmptyResultSetException e) {
				results = new ArrayList<>();
			}
			final List<Category> categories = results;
			
			entries.sort((final Entry e1, final Entry e2) -> {
				Category category1 = e1.categoryUuid == null ? null : categories.stream().filter(category -> e1.categoryUuid.equals(category.uuid)).findFirst().orElse(null);
				Category category2 = e2.categoryUuid == null ? null : categories.stream().filter(category -> e2.categoryUuid.equals(category.uuid)).findFirst().orElse(null);
				
				if (category1 != null && category2 != null) {
					int result = category1.name.compareTo(category2.name);
					if (result != 0) {
						return result;
					} else {
						return e1.displayName.compareTo(e2.displayName);
					}
				} else if (category1 == null && category2 != null) {
					return -1;
				} else if (category1 != null) {
					return 1;
				} else {
					return e1.displayName.compareTo(e2.displayName);
				}
			});
			emitter.onSuccess(entries);
		});
	}
	
}
