package hu.elte.passchest.ui.settings.editpassword;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import androidx.lifecycle.ViewModelProviders;
import hu.elte.passchest.BR;
import hu.elte.passchest.R;
import hu.elte.passchest.databinding.ActivityEditPasswordBinding;
import hu.elte.passchest.ui.base.BaseActivity;
import hu.elte.passchest.viewmodel.ViewModelProviderFactory;
import org.mindrot.jbcrypt.BCrypt;

import javax.inject.Inject;

public class EditPasswordActivity extends BaseActivity<ActivityEditPasswordBinding, EditPasswordViewModel> implements EditPasswordNavigator {
	
	@Inject
	ViewModelProviderFactory factory;
	
	private EditPasswordViewModel mViewModel;
	private ActivityEditPasswordBinding mBinding;
	
	@Override
	public int getBindingVariable() {
		return BR.viewModel;
	}
	
	@Override
	public int getLayoutId() {
		return R.layout.activity_edit_password;
	}
	
	@Override
	public EditPasswordViewModel getViewModel() {
		mViewModel = ViewModelProviders.of(this, factory).get(EditPasswordViewModel.class);
		return mViewModel;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mViewModel.setNavigator(this);
		mBinding = getViewDataBinding();
	}
	
	public void onSubmitButtonClicked(View view) {
		CharSequence currentPassword = mBinding.editPasswordEditTextCurrentPassword.getText();
		CharSequence newPassword = mBinding.editPasswordEditTextNewPassword.getText();
		CharSequence newPassword2 = mBinding.editPasswordEditTextNewPasswordAgain.getText();
		
		if (validateCurrentPassword(currentPassword) & validateNewPassword(newPassword, newPassword2)) {
			if (!mViewModel.getDataManager().getOfflineMode()) {
				mViewModel.changePassword(newPassword);
			} else {
				Toast.makeText(this, getString(R.string.error_offline_mode), Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	private boolean validateCurrentPassword(CharSequence currentPassword) {
		boolean valid = true;
		if (!BCrypt.checkpw(currentPassword.toString(), mViewModel.getCurrentPassword().getValue().toString())) {
			valid = false;
			mBinding.editPasswordEditTextCurrentPasswordContainer.setError(getString(R.string.error_input_invalid));
		} else {
			mBinding.editPasswordEditTextCurrentPasswordContainer.setError(null);
		}
		return valid;
	}
	
	private boolean validateNewPassword(CharSequence password, CharSequence password2) {
		boolean valid = true;
		if (mViewModel.isInputEmpty(password)) {
			valid = false;
			mBinding.editPasswordEditTextNewPasswordContainer.setError(getString(R.string.error_input_empty));
		} else {
			mBinding.editPasswordEditTextNewPasswordContainer.setError(null);
		}
		if (!mViewModel.isPasswordValid(password)) {
			valid = false;
			mBinding.editPasswordEditTextNewPasswordContainer.setError(getString(R.string.error_input_password_length));
		} else {
			mBinding.editPasswordEditTextNewPasswordContainer.setError(null);
		}
		if (!mViewModel.isMatchingPassword(password, password2)) {
			valid = false;
			mBinding.editPasswordEditTextNewPasswordAgainContainer.setError(getString(R.string.error_input_password_mismatch));
		} else {
			mBinding.editPasswordEditTextNewPasswordAgainContainer.setError(null);
		}
		return valid;
	}
	
	@Override
	public void onActionComplete() {
		Toast.makeText(this, getString(R.string.success_edit_password), Toast.LENGTH_SHORT).show();
		closeActivity();
	}
	
	@Override
	public void handleError(Throwable throwable) {
		mBinding.editPasswordEditTextCurrentPasswordContainer.setError(getString(R.string.error_input_invalid));
	}
	
	@Override
	public void closeActivity() {
		finish();
		playClosingAnimation();
	}
	
	private void playClosingAnimation() {
		overridePendingTransition(R.anim.fade_in, R.anim.slide_out);
	}
}
