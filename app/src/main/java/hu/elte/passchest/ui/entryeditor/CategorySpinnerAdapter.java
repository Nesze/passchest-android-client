package hu.elte.passchest.ui.entryeditor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import hu.elte.passchest.R;
import hu.elte.passchest.data.model.entity.Category;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class CategorySpinnerAdapter extends ArrayAdapter<Category> {
	
	public static final String TAG = "CategorySpinnerAdapter";
	
	private Context mContext;
	private int mResource;
	
	static class ViewHolder {
		TextView name;
	}
	
	public CategorySpinnerAdapter(Context context, int resource, List<Category> objects) {
		super(context, resource, objects);
		mContext = context;
		mResource = resource;
	}
	
	@Override
	public int getCount() {
		return super.getCount();
	}
	
	@Override
	public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
		return createItemView(position, convertView, parent);
	}
	
	@NotNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
		return createItemView(position, convertView, parent);
	}
	
	private View createItemView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(mResource, parent, false);
			holder = new ViewHolder();
			holder.name = convertView.findViewById(R.id.spinner_name);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.name.setText(getItem(position).name);
		
		return convertView;
	}
	
}
