package hu.elte.passchest.ui.addresseditor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import androidx.lifecycle.ViewModelProviders;
import hu.elte.passchest.BR;
import hu.elte.passchest.R;
import hu.elte.passchest.databinding.ActivityAddressEditorBinding;
import hu.elte.passchest.ui.base.BaseActivity;
import hu.elte.passchest.viewmodel.ViewModelProviderFactory;

import javax.inject.Inject;
import java.util.Locale;

public class AddressEditorActivity extends BaseActivity<ActivityAddressEditorBinding, AddressEditorViewModel> implements AddressEditorNavigator {
	
	@Inject
	ViewModelProviderFactory factory;
	
	private AddressEditorViewModel mViewModel;
	private ActivityAddressEditorBinding mBinding;
	
	@Override
	public int getBindingVariable() {
		return BR.viewModel;
	}
	
	@Override
	public int getLayoutId() {
		return R.layout.activity_address_editor;
	}
	
	@Override
	public AddressEditorViewModel getViewModel() {
		mViewModel = ViewModelProviders.of(this, factory).get(AddressEditorViewModel.class);
		return mViewModel;
	}
	
	public static Intent newIntent(Context context) {
		return new Intent(context, AddressEditorActivity.class);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mViewModel.setNavigator(this);
		mBinding = getViewDataBinding();
		
		mBinding.addressEditorEditTextHost.setText(mViewModel.getHost());
		mBinding.addressEditorEditTextPort.setText(String.format(Locale.getDefault(), "%d", mViewModel.getPort()));
		mBinding.addressEditorEditTextResource.setText(mViewModel.getResource());
	}
	
	public void onSubmitButtonClicked(View view) {
		CharSequence host = mBinding.addressEditorEditTextHost.getText();
		CharSequence port = mBinding.addressEditorEditTextPort.getText();
		CharSequence resource = mBinding.addressEditorEditTextResource.getText();
		if (validateHost(host) & validatePort(port) & validateResource(resource)) {
			mViewModel.setHost(host);
			mViewModel.setPort(Integer.valueOf(port.toString()));
			mViewModel.setResource(resource);
			mViewModel.changeAddress();
		}
	}
	
	private boolean validateHost(CharSequence host) {
		boolean valid = true;
		if (mViewModel.isInputEmpty(host)) {
			valid = false;
			mBinding.addressEditorEditTextHostContainer.setError(getString(R.string.error_input_empty));
		} else {
			mBinding.addressEditorEditTextHostContainer.setError(null);
		}
		if (!mViewModel.isHostValid(host)) {
			valid = false;
		}
		return valid;
	}
	
	private boolean validatePort(CharSequence port) {
		boolean valid = true;
		if (mViewModel.isInputEmpty(port)) {
			valid = false;
			mBinding.addressEditorEditTextPortContainer.setError(getString(R.string.error_input_empty));
		} else {
			mBinding.addressEditorEditTextPortContainer.setError(null);
		}
		if (!mViewModel.isValidPort(port)) {
			valid = false;
			mBinding.addressEditorEditTextPortContainer.setError(getString(R.string.error_input_invalid));
		} else {
			mBinding.addressEditorEditTextPortContainer.setError(null);
		}
		return valid;
	}
	
	private boolean validateResource(CharSequence resource) {
		boolean valid = true;
		if (mViewModel.isInputEmpty(resource)) {
			valid = false;
			mBinding.addressEditorEditTextResourceContainer.setError(getString(R.string.error_input_empty));
		} else {
			mBinding.addressEditorEditTextResourceContainer.setError(null);
		}
		if (!mViewModel.isValidResource(resource)) {
			valid = false;
			mBinding.addressEditorEditTextResourceContainer.setError(getString(R.string.error_input_invalid));
		} else {
			mBinding.addressEditorEditTextResourceContainer.setError(null);
		}
		return valid;
	}
	
	@Override
	public void onActionComplete() {
		Toast.makeText(this, getString(R.string.success_edit_address), Toast.LENGTH_SHORT).show();
		closeActivity();
	}
	
	@Override
	public void closeActivity() {
		finish();
		playClosingAnimation();
	}
	
	private void playClosingAnimation() {
		overridePendingTransition(R.anim.fade_in, R.anim.slide_out);
	}
}
