package hu.elte.passchest.ui.addresseditor;

public interface AddressEditorNavigator {
	
	void onActionComplete();
	
	void closeActivity();
	
}
