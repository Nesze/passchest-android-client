package hu.elte.passchest.ui.settings.editemail;

public interface EditEmailNavigator {
	
	void onActionComplete();
	
	void handleError(Throwable throwable);
	
	void closeActivity();
	
}
