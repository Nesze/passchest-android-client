package hu.elte.passchest.ui.auth;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;
import androidx.lifecycle.ViewModelProviders;
import com.androidnetworking.error.ANError;
import hu.elte.passchest.BR;
import hu.elte.passchest.R;
import hu.elte.passchest.databinding.ActivityAuthBinding;
import hu.elte.passchest.ui.addresseditor.AddressEditorActivity;
import hu.elte.passchest.ui.base.BaseActivity;
import hu.elte.passchest.ui.main.MainActivity;
import hu.elte.passchest.ui.registration.RegistrationActivity;
import hu.elte.passchest.utils.AppLogger;
import hu.elte.passchest.viewmodel.ViewModelProviderFactory;

import javax.inject.Inject;

public class AuthActivity extends BaseActivity<ActivityAuthBinding, AuthViewModel> implements AuthNavigator {

    @Inject
    ViewModelProviderFactory factory;

    private AuthViewModel mViewModel;
    private ActivityAuthBinding mBinding;
    
    private Boolean mIsDifferentUser;
    
    public static Intent newIntent(Context context) {
        return new Intent(context, AuthActivity.class);
    }
    
    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }
    
    @Override
    public int getLayoutId() {
        return R.layout.activity_auth;
    }
    
    @Override
    public AuthViewModel getViewModel() {
        mViewModel = ViewModelProviders.of(this, factory).get(AuthViewModel.class);
        return mViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        mViewModel.setNavigator(this);
        mBinding = getViewDataBinding();
        
        Boolean isAutofillEnabled = mViewModel.getIsUsernameAutofillEnabled();
        AppLogger.d("username autofill: %s", isAutofillEnabled ? "enabled": "disabled");

        if (isAutofillEnabled && mViewModel.getUsername().getValue() != null) {
            mBinding.authEditTextUsername.setText(mViewModel.getUsername().getValue());
            mBinding.authEditTextPassword.requestFocus();
        } else {
            mBinding.authEditTextUsername.requestFocus();
        }
        
        hideKeyboard();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actions_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_create_account) {
            openRegistrationActivity();
            return true;
        } else if (item.getItemId() == R.id.action_edit_address) {
            openAddressEditorActivity();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onSubmitButtonClicked(View view) {
        CharSequence username = mBinding.authEditTextUsername.getText();
        CharSequence password = mBinding.authEditTextPassword.getText();
        
        if (validateUsername(username) & validatePassword(password)) {
            CharSequence storedUsername = mViewModel.getUsername().getValue();
            mIsDifferentUser = storedUsername != null && !storedUsername.toString().equals(username.toString());
            login();
        }
    }
    
    private boolean validateUsername(CharSequence username) {
        boolean valid = true;
        if (!mViewModel.isUsernameValid(username)) {
            valid = false;
            mBinding.authEditTextUsernameContainer.setError(getString(R.string.error_input_empty));
        } else {
            mBinding.authEditTextUsernameContainer.setError(null);
        }
        return valid;
    }
    
    private boolean validatePassword(CharSequence password) {
        boolean valid = true;
        if (!mViewModel.isPasswordValid(password)) {
            valid = false;
            mBinding.authEditTextPasswordContainer.setError(getString(R.string.error_input_empty));
        } else {
            mBinding.authEditTextPasswordContainer.setError(null);
        }
        return valid;
    }
    
    @Override
    public void handleError(Throwable throwable) {
        if (throwable instanceof ANError) {
            if (((ANError) throwable).getErrorCode() == 0) {
                if (((ANError) throwable).getErrorDetail().equals("connectionError")) {
                    Toast.makeText(this, getString(R.string.error_network_timeout), Toast.LENGTH_SHORT).show();
                } else {
                    throw new UnknownError();
                }
            } else {
                switch (((ANError) throwable).getErrorCode()) {
                    case 401:
                        mBinding.authEditTextUsernameContainer.setError(getString(R.string.error_input_invalid));
                        mBinding.authEditTextPasswordContainer.setError(getString(R.string.error_input_invalid));
                        break;
                    case 500:
                        Toast.makeText(this, getString(R.string.error_server_error), Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        Toast.makeText(this, getString(R.string.error_unknown), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
    
    @Override
    public void login() {
        CharSequence username = mBinding.authEditTextUsername.getText();
        CharSequence password = mBinding.authEditTextPassword.getText();
        
        mViewModel.setIsOffline(!isNetworkConnected());
        mViewModel.login(username, password);
    }
    
    @Override
    public void onAuthorizationComplete() {
        if (mIsDifferentUser) {
            mViewModel.clearAllTables(this::openMainActivity);
        } else {
            openMainActivity();
        }
    }
    
    @Override
    public void openMainActivity() {
        startActivity(MainActivity.newIntent(AuthActivity.this));
        finish();
    }
    
    @Override
    public void openRegistrationActivity() {
        startActivity(RegistrationActivity.newIntent(AuthActivity.this));
        playOpeningAnimation();
    }
    
    @Override
    public void openAddressEditorActivity() {
        startActivity(AddressEditorActivity.newIntent(this));
        playOpeningAnimation();
    }
    
    private void playOpeningAnimation() {
        overridePendingTransition(R.anim.slide_in, R.anim.fade_out);
    }
    
    @Override
    public void hideKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }
    
}
