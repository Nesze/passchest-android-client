package hu.elte.passchest.ui.entryview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import hu.elte.passchest.R;
import hu.elte.passchest.data.model.entity.Category;
import hu.elte.passchest.data.model.entity.Entry;

import java.util.List;

public class EntryListAdapter extends ArrayAdapter<Entry> {
	
	public static final String TAG = "EntryListAdapter";
	
	private Context mContext;
	private int mResource;
	
	private List<Category> mCategories;
	
	static class ViewHolder {
		TextView displayName;
		TextView category;
	}
	
	public EntryListAdapter(Context context, int resource, List<Entry> objects, List<Category> categories) {
		super(context, resource, objects);
		mContext = context;
		mResource = resource;
		mCategories = categories;
	}
	
	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(mContext);
			convertView = inflater.inflate(mResource, parent, false);
			holder = new ViewHolder();
			holder.displayName = convertView.findViewById(R.id.text_display_name);
			holder.category = convertView.findViewById(R.id.text_category);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.displayName.setText(getItem(position).displayName);
		Category cat = getItem(position).categoryUuid == null ? null : mCategories.stream().filter(category -> getItem(position).categoryUuid.equals(category.uuid)).findFirst().orElse(null);
		holder.category.setText(cat != null ? cat.name : "");
		
		return convertView;
	}
}
