package hu.elte.passchest.ui.categoryeditor;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import hu.elte.passchest.data.DataManager;
import hu.elte.passchest.data.model.api.entrydatabase.Status;
import hu.elte.passchest.data.model.entity.Category;
import hu.elte.passchest.ui.base.BaseViewModel;
import hu.elte.passchest.utils.rx.SchedulerProvider;

import java.util.Date;
import java.util.UUID;

public class CategoryEditorViewModel extends BaseViewModel<CategoryEditorNavigator> {
	
	MutableLiveData<Category> mCategory;
	
	public CategoryEditorViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
		super(dataManager, schedulerProvider);
	}
	
	public LiveData<Category> getCategory() {
		return getCategory(null);
	}
	
	public LiveData<Category> getCategory(UUID uuid) {
		if (mCategory == null) {
			mCategory = new MutableLiveData<>();
			if (uuid != null) {
				loadCategory(uuid);
			}
		}
		return mCategory;
	}

	public void updateCategory(CharSequence name, Date dateModified) {
		if (mCategory.getValue() == null) {
			mCategory.setValue(new Category());
		}
		Category category = mCategory.getValue();
		category.name = name.toString();
		category.status = Status.ACTIVE;
		category.dateModified = dateModified;
		mCategory.setValue(category);
	}
	
	private void loadCategory(UUID uuid) {
		setIsLoading(true);
		getCompositeDisposable()
				.add(getDataManager()
						.getCategoryByUuid(uuid)
						.subscribeOn(getSchedulerProvider().io())
						.observeOn(getSchedulerProvider().ui())
						.subscribe(category -> {
							mCategory.setValue(category);
							setIsLoading(false);
						}, throwable -> setIsLoading(false))
				);
	}
	
	public void saveCategory() {
		setIsLoading(true);
		getCompositeDisposable()
				.add(getDataManager()
						.insertCategory(mCategory.getValue())
						.subscribeOn(getSchedulerProvider().io())
						.observeOn(getSchedulerProvider().ui())
						.subscribe(() -> {
							getNavigator().onSaveCompleted();
							setIsLoading(false);
						})
				);
	}
	
}
