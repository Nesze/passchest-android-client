package hu.elte.passchest.di.builder;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import hu.elte.passchest.ui.addresseditor.AddressEditorActivity;
import hu.elte.passchest.ui.auth.AuthActivity;
import hu.elte.passchest.ui.categoryeditor.CategoryEditorActivity;
import hu.elte.passchest.ui.entryeditor.EntryEditorActivity;
import hu.elte.passchest.ui.main.MainActivity;
import hu.elte.passchest.ui.registration.RegistrationActivity;
import hu.elte.passchest.ui.settings.editemail.EditEmailActivity;
import hu.elte.passchest.ui.settings.editpassword.EditPasswordActivity;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract AuthActivity bindAuthActivity();
    
    @ContributesAndroidInjector
    abstract AddressEditorActivity bindAddressActivity();

    @ContributesAndroidInjector
    abstract RegistrationActivity bindRegistrationActivity();
    
    @ContributesAndroidInjector
    abstract MainActivity bindMainActivity();

    @ContributesAndroidInjector
    abstract EntryEditorActivity bindEntryEditorActivity();
    
    @ContributesAndroidInjector
    abstract CategoryEditorActivity bindCategoryEditorActivity();
    
    @ContributesAndroidInjector
    abstract EditEmailActivity bindEditEmailActivity();
    
    @ContributesAndroidInjector
    abstract EditPasswordActivity bindEditPasswordActivity();
    
}
