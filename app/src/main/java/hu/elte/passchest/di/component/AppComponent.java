package hu.elte.passchest.di.component;

import android.app.Application;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import hu.elte.passchest.PassChestApplication;
import hu.elte.passchest.di.builder.ActivityBuilder;
import hu.elte.passchest.di.module.AppModule;
import hu.elte.passchest.di.builder.FragmentBuilder;

import javax.inject.Singleton;

@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AppModule.class,
        ActivityBuilder.class,
        FragmentBuilder.class})
public interface AppComponent {

    void inject(PassChestApplication app);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        AppComponent build();

    }

}
