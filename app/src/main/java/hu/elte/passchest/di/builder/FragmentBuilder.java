package hu.elte.passchest.di.builder;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import hu.elte.passchest.ui.categoryview.CategoryViewFragment;
import hu.elte.passchest.ui.entryview.EntryViewFragment;

@Module
public abstract class FragmentBuilder {
	
	@ContributesAndroidInjector
	abstract EntryViewFragment bindEntryViewFragment();
	
	@ContributesAndroidInjector
	abstract CategoryViewFragment bindCategoryViewFragment();
	
}
