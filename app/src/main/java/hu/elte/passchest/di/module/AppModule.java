package hu.elte.passchest.di.module;

import android.app.Application;
import android.content.Context;
import androidx.room.Room;
import dagger.Module;
import dagger.Provides;
import hu.elte.passchest.data.AppDataManager;
import hu.elte.passchest.data.DataManager;
import hu.elte.passchest.data.local.db.AppDatabase;
import hu.elte.passchest.data.local.db.AppDbHelper;
import hu.elte.passchest.data.local.db.DbHelper;
import hu.elte.passchest.data.local.prefs.AppPreferencesHelper;
import hu.elte.passchest.data.local.prefs.PreferencesHelper;
import hu.elte.passchest.data.remote.ApiHelper;
import hu.elte.passchest.data.remote.AppApiHelper;
import hu.elte.passchest.di.DatabaseInfo;
import hu.elte.passchest.di.PreferenceInfo;
import hu.elte.passchest.utils.AppConstants;
import hu.elte.passchest.utils.rx.AppSchedulerProvider;
import hu.elte.passchest.utils.rx.SchedulerProvider;

import javax.inject.Singleton;

@Module
public class AppModule {
    
    @Provides
    @Singleton
    ApiHelper provideApiHelper(AppApiHelper appApiHelper) {
        return appApiHelper;
    }
    
    @Provides
    @Singleton
    AppDatabase provideAppDatabase(@DatabaseInfo String dbName, Context context) {
        return Room.databaseBuilder(context, AppDatabase.class, dbName)
//                .addMigrations(MIGRATION_1_2) - example
                .build();
    }
    
    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }
    
    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }
    
    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return AppConstants.DB_NAME;
    }
    
    @Provides
    @Singleton
    DbHelper provideDbHelper(AppDbHelper appDbHelper) {
        return appDbHelper;
    }
    
    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }
    
    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }
    
    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

}
