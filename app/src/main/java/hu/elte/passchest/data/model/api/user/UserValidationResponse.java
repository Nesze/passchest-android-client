package hu.elte.passchest.data.model.api.user;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserValidationResponse {
	
	public enum Status { OK, INVALID, OCCUPIED }
	
	@SerializedName("emailStatus")
	private UserValidationResponse.Status emailStatus;
	
	@SerializedName("usernameStatus")
	private UserValidationResponse.Status usernameStatus;
	
	@SerializedName("passwordStatus")
	private UserValidationResponse.Status passwordStatus;
	
}
