package hu.elte.passchest.data.model.api.entrydatabase;

public enum Status {ACTIVE, DELETED}
