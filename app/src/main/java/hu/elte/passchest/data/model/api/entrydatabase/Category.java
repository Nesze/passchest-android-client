package hu.elte.passchest.data.model.api.entrydatabase;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.UUID;

public class Category {

    @JsonProperty("uuid")
    public UUID uuid;
    
    @JsonProperty("status")
    public Status status;

    @JsonProperty("name")
    public String name;
    
    @JsonFormat(
            shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    @JsonProperty("dateModified")
    public Date dateModified;

}
