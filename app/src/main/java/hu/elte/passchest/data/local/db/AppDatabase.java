package hu.elte.passchest.data.local.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import hu.elte.passchest.data.local.db.dao.CategoryDao;
import hu.elte.passchest.data.local.db.dao.EntryDao;
import hu.elte.passchest.data.model.entity.Category;
import hu.elte.passchest.data.model.entity.Entry;
import hu.elte.passchest.utils.Converters;

@TypeConverters({Converters.class})
@Database(entities = {Entry.class, Category.class}, version = 2, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

	public abstract EntryDao entryDao();

	public abstract CategoryDao categoryDao();

}
