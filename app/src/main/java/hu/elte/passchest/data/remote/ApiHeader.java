package hu.elte.passchest.data.remote;

import android.util.Base64;

import javax.inject.Singleton;

@Singleton
public class ApiHeader {
	
	public String username;
	
	public String password;
	
	private static String mAuthorizationHeaderKey = "Authorization";
	
	public ApiHeader(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	public String getAuthorizationHeaderValue() {
		String credentials = username + ":" + password;
		return "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
	}
	
	public String getAuthorizationHeaderKey() {
		return mAuthorizationHeaderKey;
	}

}
