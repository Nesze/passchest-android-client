package hu.elte.passchest.data.local.db;

import hu.elte.passchest.data.model.api.entrydatabase.EntryDatabase;
import hu.elte.passchest.data.model.entity.Category;
import hu.elte.passchest.data.model.entity.Entry;
import io.reactivex.Completable;
import io.reactivex.Single;

import java.util.List;
import java.util.UUID;

public interface DbHelper {

	// GET
	
	Single<List<Entry>> getAllEntries();
	
	Single<List<Entry>> getAllEntriesForExport();
	
	Single<Entry> getEntryByUuid(final UUID uuid);
	
	Single<List<Entry>> getEntriesByUrl(final CharSequence url);
	
	Single<List<Entry>> getEntriesByDisplayName(final CharSequence displayName);
	
	Single<List<Entry>> getEntriesByCategoryUuid(final UUID categoryUuid);
	
	Single<List<Category>> getAllCategories();
	
	Single<List<Category>> getAllCategoriesForExport();
	
	Single<Category> getCategoryByUuid(final UUID uuid);
	
	Single<List<Category>> getCategoriesByName(final CharSequence name);
	
	Completable mergeFromEntryDatabase(EntryDatabase entryDatabase);
	
	Single<EntryDatabase> getDatabaseForExport();
	
	// INSERT - UPDATE
	
	Completable insertEntry(final Entry entry);
	
	Completable insertCategory(final Category category);
	
	Completable insertEntryList(final List<Entry> entries);
	
	Completable insertCategoryList(final List<Category> categories);
	
	// DELETE
	
	Completable markEntryAsDeleted(final Entry entry);
	
	Completable markCategoryAsDeleted(final Category category);
	
	Completable clearAllTables();
	
}
