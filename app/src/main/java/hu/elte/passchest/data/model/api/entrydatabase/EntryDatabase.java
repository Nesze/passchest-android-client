package hu.elte.passchest.data.model.api.entrydatabase;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class EntryDatabase {

    public EntryDatabase() {
        categories = new ArrayList<>();
        entries = new ArrayList<>();
    }
    
    @JsonProperty("categories")
    public List<Category> categories;

    @JsonProperty("entries")
    public List<Entry> entries;

}
