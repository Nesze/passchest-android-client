package hu.elte.passchest.data.model.api.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRequest {
    
    public UserRequest(String email, String username, String password) {
        this.email = email;
        this.username = username;
        this.password = password;
    }

    //@NotEmpty
    //@Email(message = "invalid username")
    @JsonProperty("email")
    private String email;

    //@Size(min = 3, max = 32, message = "invalid username length")
    @JsonProperty("username")
    private String username;

    //@Size(min = 8, message = "invalid password length")
    @JsonProperty("password")
    private String password;

}
