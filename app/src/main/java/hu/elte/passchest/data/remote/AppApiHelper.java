package hu.elte.passchest.data.remote;

import com.google.gson.*;
import com.rx2androidnetworking.Rx2AndroidNetworking;
import hu.elte.passchest.data.model.api.entrydatabase.Category;
import hu.elte.passchest.data.model.api.entrydatabase.Entry;
import hu.elte.passchest.data.model.api.entrydatabase.EntryDatabase;
import hu.elte.passchest.data.model.api.user.UserDetailsResponse;
import hu.elte.passchest.data.model.api.user.UserRequest;
import hu.elte.passchest.data.model.api.user.UserValidationResponse;
import hu.elte.passchest.utils.AppConstants;
import io.reactivex.Single;
import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.lang.reflect.Type;

@Singleton
public class AppApiHelper implements ApiHelper, JsonSerializer {
	
	private ApiHeader mApiHeader;
	private String mServerAddress;

	@Inject
	public AppApiHelper() {
	
	}
	
	private String getFullAddress(String endpoint) {
		return mServerAddress + endpoint;
	}
	
	@Override
	public void setHeaderDetails(CharSequence username, CharSequence password) {
		mApiHeader = new ApiHeader(username.toString(), password.toString());
	}
	
	@Override
	public void setCurrentServerAddress(CharSequence address) {
		mServerAddress = address.toString();
	}
	
	@Override
	public CharSequence getPassword() {
		return mApiHeader.password;
	}
	
	@Override
	public Single<UserDetailsResponse> doUserDetailsApiCall() {
		return Rx2AndroidNetworking.get(getFullAddress(ApiEndPoint.ENDPOINT_GET_USER))
				.addHeaders(mApiHeader.getAuthorizationHeaderKey(), mApiHeader.getAuthorizationHeaderValue())
				.build()
				.getObjectSingle(UserDetailsResponse.class);
	}
	
	@Override
	public Single<UserValidationResponse> doUserRegistrationApiCall(UserRequest request) {
		return Rx2AndroidNetworking.put(getFullAddress(ApiEndPoint.ENDPOINT_REGISTER_USER))
				.addApplicationJsonBody(request)
				.build()
				.getObjectSingle(UserValidationResponse.class);
	}
	
	@Override
	public Single<UserValidationResponse> doEditEmailApiCall(UserRequest request) {
		return Rx2AndroidNetworking.post(getFullAddress(ApiEndPoint.ENDPOINT_EDIT_EMAIL))
				.addHeaders(mApiHeader.getAuthorizationHeaderKey(), mApiHeader.getAuthorizationHeaderValue())
				.addApplicationJsonBody(request)
				.build()
				.getObjectSingle(UserValidationResponse.class);
	}
	
	@Override
	public Single<UserValidationResponse> doEditPasswordApiCall(UserRequest request) {
		return Rx2AndroidNetworking.post(getFullAddress(ApiEndPoint.ENDPOINT_EDIT_PASSWORD))
				.addHeaders(mApiHeader.getAuthorizationHeaderKey(), mApiHeader.getAuthorizationHeaderValue())
				.addApplicationJsonBody(request)
				.build()
				.getObjectSingle(UserValidationResponse.class);
	}
	
	@Override
	public Single<EntryDatabase> doDownloadEntryDbApiCall() {
		return Rx2AndroidNetworking.get(getFullAddress(ApiEndPoint.ENDPOINT_GET_ENTRYDB))
				.addHeaders(mApiHeader.getAuthorizationHeaderKey(), mApiHeader.getAuthorizationHeaderValue())
				.build()
				.getObjectSingle(EntryDatabase.class);
	}
	
	@Override
	public Single<UserValidationResponse> doUploadEntryDbApiCall(EntryDatabase entryDatabase) {
		try {
			Gson gson = new GsonBuilder().setDateFormat(AppConstants.TIMESTAMP_FORMAT).create();
			JSONObject object = new JSONObject(gson.toJson(entryDatabase));
			return Rx2AndroidNetworking.post(getFullAddress(ApiEndPoint.ENDPOINT_UPLOAD_ENTRYDB))
					.addHeaders(mApiHeader.getAuthorizationHeaderKey(), mApiHeader.getAuthorizationHeaderValue())
					.addJSONObjectBody(object)
					.build()
					.getObjectSingle(UserValidationResponse.class);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	@Override
	public JsonElement serialize(Object src, Type typeOfSrc, JsonSerializationContext context) {
		JsonObject jsonObject = new JsonObject();
		if (typeOfSrc == Entry.class) {
			Entry source = (Entry) src;
			jsonObject.addProperty("uuid", source.uuid.toString());
			jsonObject.addProperty("status", source.uuid.toString());
			jsonObject.addProperty("uuid", source.uuid.toString());
			jsonObject.addProperty("uuid", source.uuid.toString());
			jsonObject.addProperty("uuid", source.uuid.toString());
			jsonObject.addProperty("uuid", source.uuid.toString());
			jsonObject.addProperty("uuid", source.uuid.toString());
		} else if (typeOfSrc == Category.class) {
			Category source = (Category) src;
		} else if (typeOfSrc == EntryDatabase.class) {
			EntryDatabase source = (EntryDatabase) src;
		}
		return null;
	}
}
