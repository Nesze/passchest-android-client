package hu.elte.passchest.data.model.api.entrydatabase;

import com.fasterxml.jackson.annotation.*;

import java.util.Date;
import java.util.UUID;

public class Entry {

    @JsonProperty("uuid")
    public UUID uuid;
    
    @JsonProperty("status")
    public Status status;

    @JsonProperty("categoryUuid")
    public UUID categoryUuid;

    @JsonProperty("displayName")
    public String displayName;

    @JsonProperty("url")
    public String url;

    @JsonProperty("username")
    public String username;

    @JsonProperty("password")
    public String password;

    @JsonFormat(
            shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    @JsonProperty("dateModified")
    public Date dateModified;

}
