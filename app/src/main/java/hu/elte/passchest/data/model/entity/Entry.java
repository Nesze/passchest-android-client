package hu.elte.passchest.data.model.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import hu.elte.passchest.data.model.api.entrydatabase.Status;

import java.util.Date;
import java.util.UUID;

@Entity(tableName = "entries")
public class Entry {

	@PrimaryKey
	@NonNull
	@ColumnInfo(name = "uuid")
	public UUID uuid;
	
	@ColumnInfo(name = "status")
	public Status status;

	@ColumnInfo(name = "category_uuid")
	public UUID categoryUuid;
	
	@ColumnInfo(name = "display_name")
	public String displayName;

	@ColumnInfo(name = "url")
	public String url;

	@ColumnInfo(name = "username")
	public String username;

	@ColumnInfo(name = "password")
	public String password;

	@ColumnInfo(name = "date_modified")
	public Date dateModified;

}
