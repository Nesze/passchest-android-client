package hu.elte.passchest.data.remote;

public final class ApiEndPoint {

	public static final String ENDPOINT_GET_USER = "/user";

	public static final String ENDPOINT_REGISTER_USER = "/user/register";

	public static final String ENDPOINT_EDIT_EMAIL = "/user/editemail";

	public static final String ENDPOINT_EDIT_PASSWORD = "/user/editpassword";

	public static final String ENDPOINT_GET_ENTRYDB = "/entrydb";

	public static final String ENDPOINT_UPLOAD_ENTRYDB = "/entrydb/upload";
	
	private ApiEndPoint() {

	}

}
