package hu.elte.passchest.data.local.db.dao;

import androidx.room.*;
import hu.elte.passchest.data.model.entity.Category;
import io.reactivex.Completable;
import io.reactivex.Single;

import java.util.List;
import java.util.UUID;

@Dao
public interface CategoryDao {
	
	// SELECT

	@Query("SELECT * FROM categories WHERE uuid = :uuid")
	Single<Category> findByUuid(UUID uuid);

	@Query("SELECT * FROM categories WHERE name = :name and status != 'DELETED'")
	Single<List<Category>> findByName(CharSequence name);

	@Query("SELECT * FROM categories WHERE status != 'DELETED'")
	Single<List<Category>> loadAll();
	
	@Query("SELECT * FROM categories")
	Single<List<Category>> loadTable();
	
	// INSERT - UPDATE
	
	@Insert(onConflict = OnConflictStrategy.REPLACE)
	Completable insert(Category category);
	
	@Insert(onConflict = OnConflictStrategy.REPLACE)
	Completable insertAll(List<Category> categories);

	// DELETE
	
	@Update
	Completable update(Category category);
	
}
