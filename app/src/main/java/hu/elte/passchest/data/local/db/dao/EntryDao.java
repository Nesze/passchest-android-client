package hu.elte.passchest.data.local.db.dao;

import androidx.room.*;
import hu.elte.passchest.data.model.entity.Entry;
import io.reactivex.Completable;
import io.reactivex.Single;

import java.util.List;
import java.util.UUID;

@Dao
public interface EntryDao {
	
	// SELECT
	
	@Query("SELECT * FROM entries WHERE uuid = :uuid")
	Single<Entry> findByUuid(UUID uuid);

	@Query("SELECT * FROM entries WHERE url = :url and status != 'DELETED'")
	Single<List<Entry>> findByUrl(CharSequence url);

	@Query("SELECT * FROM entries WHERE display_name = :displayName and status != 'DELETED'")
	Single<List<Entry>> findByDisplayName(CharSequence displayName);

	@Query("SELECT * FROM entries WHERE category_uuid = :catUuid and status != 'DELETED'")
	Single<List<Entry>> findByCategoryId(UUID catUuid);

	@Query("SELECT * FROM entries WHERE status != 'DELETED'")
	Single<List<Entry>> loadAll();
	
	@Query("SELECT * FROM entries")
	Single<List<Entry>> loadTable();
	
	// INSERT - UPDATE
	
	@Insert(onConflict = OnConflictStrategy.REPLACE)
	Completable insert(Entry entry);
	
	@Insert(onConflict = OnConflictStrategy.REPLACE)
	Completable insertAll(List<Entry> entries);
	
	// DELETE
	
	@Update
	Completable update(Entry entry);

}
