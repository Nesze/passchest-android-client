package hu.elte.passchest.data.model.api.user;

import androidx.room.TypeConverters;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import hu.elte.passchest.utils.Converters;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@TypeConverters({Converters.class})
@Getter @Setter
public class UserDetailsResponse {

	@Expose
	@SerializedName("username")
	private String username;
	
	@Expose
	@SerializedName("email")
	private String email;
	
	@Expose
	@SerializedName("entryDbDate")
	private Date entryDbDate;

}
