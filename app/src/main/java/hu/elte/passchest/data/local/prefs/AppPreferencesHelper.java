/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package hu.elte.passchest.data.local.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import hu.elte.passchest.di.PreferenceInfo;
import hu.elte.passchest.utils.Converters;

import javax.inject.Inject;
import java.util.Date;

/**
 * Created by amitshekhar on 07/07/17.
 */

public class AppPreferencesHelper implements PreferencesHelper {

    // Preference key constants
    private static final String PREF_KEY_USER_NAME = "PREF_KEY_USER_NAME";
    private static final String PREF_KEY_USER_EMAIL = "PREF_KEY_USER_EMAIL";
    private static final String PREF_KEY_ENTRY_DB_DATE = "PREF_KEY_ENTRY_DB_DATE";
    private static final String PREF_KEY_REMEMBER_USERNAME = "PREF_KEY_REMEMBER_USERNAME";
    private static final String PREF_KEY_AUTO_SYNC = "PREF_KEY_AUTO_SYNC";
    private static final String PREF_KEY_PASSWORD_HASH = "PREF_KEY_PASSWORD_HASH";
    private static final String PREF_KEY_SERVER_ADDRESS = "PREF_KEY_SERVER_ADDRESS";

    // Data access
    private final SharedPreferences mPrefs;
    private final SharedPreferences mDefaultPrefs;

    @Inject
    public AppPreferencesHelper(Context context, @PreferenceInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
        mDefaultPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Override
    public CharSequence getUsername() {
        return mPrefs.getString(PREF_KEY_USER_NAME, null);
    }
    
    @Override
    public CharSequence getEmail() {
        return mPrefs.getString(PREF_KEY_USER_EMAIL, null);
    }
    
    @Override
    public Date getEntryDbDate() {
        String dateText = mPrefs.getString(PREF_KEY_ENTRY_DB_DATE, null);
        if (dateText == null) {
            return null;
        }
        return Converters.stringToDate(dateText);
    }
    
    @Override
    public Boolean getIsUsernameAutofillEnabled() {
        return mDefaultPrefs.getBoolean(PREF_KEY_REMEMBER_USERNAME, true);
    }
    
    @Override
    public Boolean getIsAutoSyncEnabled() {
        return mDefaultPrefs.getBoolean(PREF_KEY_AUTO_SYNC, true);
    }
    
    @Override
    public CharSequence getPasswordHash() {
        return mPrefs.getString(PREF_KEY_PASSWORD_HASH, "");
    }
    
    @Override
    public CharSequence getServerAddress() {
        return mPrefs.getString(PREF_KEY_SERVER_ADDRESS, "");
    }
    
    @Override
    public void setUsername(CharSequence userName) {
        mPrefs.edit().putString(PREF_KEY_USER_NAME, userName.toString()).apply();
    }
    
    @Override
    public void setEmail(CharSequence email) {
        mPrefs.edit().putString(PREF_KEY_USER_EMAIL, email.toString()).apply();
    }
    
    @Override
    public void setEntryDbDate(Date entryDbDate) {
        String dateText = Converters.dateToString(entryDbDate);
        mPrefs.edit().putString(PREF_KEY_ENTRY_DB_DATE, dateText).apply();
    }
    
    @Override
    public void setUserDetails(CharSequence username, CharSequence email, Date entryDbDate) {
        setUsername(username);
        setEmail(email);
        setEntryDbDate(entryDbDate);
    }
    
    @Override
    public void setPasswordHash(CharSequence hash) {
        mPrefs.edit().putString(PREF_KEY_PASSWORD_HASH, hash.toString()).apply();
    }
    
    @Override
    public void setServerAddress(CharSequence address) {
        mPrefs.edit().putString(PREF_KEY_SERVER_ADDRESS, address.toString()).apply();
    }
    
}
