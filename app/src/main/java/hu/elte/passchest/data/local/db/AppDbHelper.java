package hu.elte.passchest.data.local.db;

import androidx.room.EmptyResultSetException;
import hu.elte.passchest.data.model.api.entrydatabase.EntryDatabase;
import hu.elte.passchest.data.model.api.entrydatabase.Status;
import hu.elte.passchest.data.model.entity.Category;
import hu.elte.passchest.data.model.entity.Entry;
import io.reactivex.Completable;
import io.reactivex.Single;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class AppDbHelper implements DbHelper {

	private final AppDatabase mAppDatabase;

	@Inject
	public AppDbHelper(AppDatabase appDatabase) {
		this.mAppDatabase = appDatabase;
	}

	@Override
	public Single<List<Entry>> getAllEntries() {
		return mAppDatabase.entryDao().loadAll();
	}
	
	@Override
	public Single<List<Entry>> getAllEntriesForExport() {
		return mAppDatabase.entryDao().loadTable();
	}
	
	@Override
	public Single<Entry> getEntryByUuid(final UUID id) {
		return mAppDatabase.entryDao().findByUuid(id);
	}

	@Override
	public Single<List<Entry>> getEntriesByUrl(final CharSequence url) {
		return mAppDatabase.entryDao().findByUrl(url);
	}

	@Override
	public Single<List<Entry>> getEntriesByDisplayName(final CharSequence displayName) {
		return mAppDatabase.entryDao().findByDisplayName(displayName);
	}

	@Override
	public Single<List<Entry>> getEntriesByCategoryUuid(final UUID categoryUuid) {
		return mAppDatabase.entryDao().findByCategoryId(categoryUuid);
	}

	@Override
	public Single<List<Category>> getAllCategories() {
		return mAppDatabase.categoryDao().loadAll();
	}
	
	@Override
	public Single<List<Category>> getAllCategoriesForExport() {
		return mAppDatabase.categoryDao().loadTable();
	}
	
	@Override
	public Single<Category> getCategoryByUuid(final UUID uuid) {
		return mAppDatabase.categoryDao().findByUuid(uuid);
	}

	@Override
	public Single<List<Category>> getCategoriesByName(final CharSequence name) {
		return mAppDatabase.categoryDao().findByName(name);
	}
	
	@Override
	public Completable mergeFromEntryDatabase(EntryDatabase entryDatabase) {
		return Completable.create(emitter -> {
			List<Entry> entriesToSave = new ArrayList<>();
			for (hu.elte.passchest.data.model.api.entrydatabase.Entry entry : entryDatabase.entries) {
				Entry localEntry;
				try {
					localEntry = mAppDatabase.entryDao().findByUuid(entry.uuid).blockingGet();
				} catch (EmptyResultSetException e) {
					localEntry = null;
				}
				if (localEntry == null || entry.status == Status.ACTIVE && entry.dateModified.after(localEntry.dateModified)) {
					entriesToSave.add(convertToLocalEntry(entry));
				}
			}
			mAppDatabase.entryDao().insertAll(entriesToSave).blockingAwait();
			
			List<Category> categoriesToSave = new ArrayList<>();
			for (hu.elte.passchest.data.model.api.entrydatabase.Category category : entryDatabase.categories) {
				Category localCategory;
				try {
					localCategory = mAppDatabase.categoryDao().findByUuid(category.uuid).blockingGet();
				} catch (EmptyResultSetException e) {
					localCategory = null;
				}
				if (localCategory == null || category.status == Status.ACTIVE && category.dateModified.after(localCategory.dateModified)) {
					categoriesToSave.add(convertToLocalCategory(category));
				}
			}
			mAppDatabase.categoryDao().insertAll(categoriesToSave).blockingAwait();
			
			emitter.onComplete();
		});
	}
	
	private Entry convertToLocalEntry(@NotNull hu.elte.passchest.data.model.api.entrydatabase.Entry entryDto) {
		Entry entry = new Entry();
		entry.uuid = entryDto.uuid;
		entry.status = entryDto.status;
		entry.displayName = entryDto.displayName;
		entry.url = entryDto.url;
		entry.categoryUuid = entryDto.categoryUuid;
		entry.username = entryDto.username;
		entry.password = entryDto.password;
		entry.dateModified = entryDto.dateModified;
		return entry;
	}
	
	private Category convertToLocalCategory(@NotNull hu.elte.passchest.data.model.api.entrydatabase.Category categoryDto) {
		Category category = new Category();
		category.uuid = categoryDto.uuid;
		category.status = categoryDto.status;
		category.name = categoryDto.name;
		category.dateModified = categoryDto.dateModified;
		return category;
	}
	
	@Override
	public Single<EntryDatabase> getDatabaseForExport() {
		return Single.create(emitter -> {
			List<Category> categories;
			try {
				categories = mAppDatabase.categoryDao().loadTable().blockingGet();
			} catch (EmptyResultSetException e) {
				categories = new ArrayList<>();
			}
			
			List<Entry> entries;
			try {
				entries = mAppDatabase.entryDao().loadTable().blockingGet();
			} catch (EmptyResultSetException e) {
				entries = new ArrayList<>();
			}
			
			EntryDatabase entryDatabase = new EntryDatabase();
			for (Entry entry : entries) {
				entryDatabase.entries.add(convertToEntryDto(entry));
			}
			for (Category category : categories) {
				entryDatabase.categories.add(convertToCategoryDto(category));
			}
			
			emitter.onSuccess(entryDatabase);
		});
	}
	
	private hu.elte.passchest.data.model.api.entrydatabase.Entry convertToEntryDto(@NotNull Entry entry) {
		hu.elte.passchest.data.model.api.entrydatabase.Entry entryDto = new hu.elte.passchest.data.model.api.entrydatabase.Entry();
		entryDto.uuid = entry.uuid;
		entryDto.status = entry.status;
		entryDto.displayName = entry.displayName;
		entryDto.url = entry.url;
		entryDto.categoryUuid = entry.categoryUuid;
		entryDto.username = entry.username;
		entryDto.password = entry.password;
		entryDto.dateModified = entry.dateModified;
		return entryDto;
	}
	
	private hu.elte.passchest.data.model.api.entrydatabase.Category convertToCategoryDto(@NotNull Category category) {
		hu.elte.passchest.data.model.api.entrydatabase.Category categoryDto = new hu.elte.passchest.data.model.api.entrydatabase.Category();
		categoryDto.uuid = category.uuid;
		categoryDto.status = category.status;
		categoryDto.name = category.name;
		categoryDto.dateModified = category.dateModified;
		return categoryDto;
	}
	
	@Override
	public Completable insertEntry(final Entry entry) {
		return mAppDatabase.entryDao().insert(entry);
	}

	@Override
	public Completable insertCategory(final Category category) {
		return mAppDatabase.categoryDao().insert(category);
	}

	@Override
	public Completable insertEntryList(final List<Entry> entries) {
		return mAppDatabase.entryDao().insertAll(entries);
	}

	@Override
	public Completable insertCategoryList(final List<Category> categories) {
		return mAppDatabase.categoryDao().insertAll(categories);
	}
	
	@Override
	public Completable markEntryAsDeleted(final Entry entry) {
		entry.status = Status.DELETED;
		entry.dateModified = new Date();
		return mAppDatabase.entryDao().update(entry);
	}
	
	@Override
	public Completable markCategoryAsDeleted(final Category category) {
		category.status = Status.DELETED;
		category.dateModified = new Date();
		return mAppDatabase.categoryDao().update(category);
	}
	
	@Override
	public Completable clearAllTables() {
		return Completable.create(emitter -> {
			mAppDatabase.clearAllTables();
			emitter.onComplete();
		});
	}
	
}
