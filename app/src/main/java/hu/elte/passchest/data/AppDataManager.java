package hu.elte.passchest.data;

import android.content.Context;
import hu.elte.passchest.data.local.db.DbHelper;
import hu.elte.passchest.data.local.prefs.PreferencesHelper;
import hu.elte.passchest.data.model.api.entrydatabase.EntryDatabase;
import hu.elte.passchest.data.model.api.user.UserDetailsResponse;
import hu.elte.passchest.data.model.api.user.UserRequest;
import hu.elte.passchest.data.model.api.user.UserValidationResponse;
import hu.elte.passchest.data.model.entity.Category;
import hu.elte.passchest.data.model.entity.Entry;
import hu.elte.passchest.data.remote.ApiHelper;
import hu.elte.passchest.utils.AppConstants;
import io.reactivex.Completable;
import io.reactivex.Single;
import org.mindrot.jbcrypt.BCrypt;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class AppDataManager implements DataManager {
	
	private final Context mContext;
	private final DbHelper mDbHelper;
	private final PreferencesHelper mPreferencesHelper;
	private final ApiHelper mApiHelper;
	
	private Date lastSync;
	private Boolean mOfflineMode;
	
	@Inject
	public AppDataManager(Context context, DbHelper dbHelper, PreferencesHelper preferencesHelper, ApiHelper apiHelper) {
		mContext = context;
		mDbHelper = dbHelper;
		mPreferencesHelper = preferencesHelper;
		mApiHelper = apiHelper;
		
		if (mPreferencesHelper.getServerAddress() == "") {
			mPreferencesHelper.setServerAddress(AppConstants.DEFAULT_ADDRESS);
			mApiHelper.setCurrentServerAddress(AppConstants.DEFAULT_ADDRESS);
		} else {
			mApiHelper.setCurrentServerAddress(mPreferencesHelper.getServerAddress());
		}
	}
	
	@Override
	public Single<List<Entry>> getAllEntries() {
		return mDbHelper.getAllEntries();
	}
	
	@Override
	public Single<List<Entry>> getAllEntriesForExport() {
		return mDbHelper.getAllEntriesForExport();
	}
	
	@Override
	public Single<Entry> getEntryByUuid(UUID uuid) {
		return mDbHelper.getEntryByUuid(uuid);
	}
	
	@Override
	public Single<List<Entry>> getEntriesByUrl(CharSequence url) {
		return mDbHelper.getEntriesByUrl(url);
	}
	
	@Override
	public Single<List<Entry>> getEntriesByDisplayName(CharSequence displayName) {
		return mDbHelper.getEntriesByDisplayName(displayName);
	}
	
	@Override
	public Single<List<Entry>> getEntriesByCategoryUuid(UUID categoryUuid) {
		return mDbHelper.getEntriesByCategoryUuid(categoryUuid);
	}
	
	@Override
	public Single<List<Category>> getAllCategories() {
		return mDbHelper.getAllCategories();
	}
	
	@Override
	public Single<List<Category>> getAllCategoriesForExport() {
		return mDbHelper.getAllCategoriesForExport();
	}
	
	@Override
	public Single<Category> getCategoryByUuid(UUID uuid) {
		return mDbHelper.getCategoryByUuid(uuid);
	}
	
	@Override
	public Single<List<Category>> getCategoriesByName(CharSequence name) {
		return mDbHelper.getCategoriesByName(name);
	}
	
	@Override
	public Completable mergeFromEntryDatabase(EntryDatabase entryDatabase) {
		setLastSyncDate(new Date());
		return mDbHelper.mergeFromEntryDatabase(entryDatabase);
	}
	
	@Override
	public Single<EntryDatabase> getDatabaseForExport() {
		return mDbHelper.getDatabaseForExport();
	}
	
	@Override
	public Completable insertEntry(Entry entry) {
		if (entry.uuid == null) {
			entry.uuid = UUID.randomUUID();
		}
		return mDbHelper.insertEntry(entry);
	}
	
	@Override
	public Completable insertCategory(Category category) {
		if (category.uuid == null) {
			category.uuid = UUID.randomUUID();
		}
		return mDbHelper.insertCategory(category);
	}
	
	@Override
	public Completable insertEntryList(List<Entry> entries) {
		return mDbHelper.insertEntryList(entries);
	}
	
	@Override
	public Completable insertCategoryList(List<Category> categories) {
		return mDbHelper.insertCategoryList(categories);
	}
	
	@Override
	public Completable markEntryAsDeleted(Entry entry) {
		return mDbHelper.markEntryAsDeleted(entry);
	}
	
	@Override
	public Completable markCategoryAsDeleted(Category category) {
		return mDbHelper.markCategoryAsDeleted(category);
	}
	
	@Override
	public CharSequence getUsername() {
		return mPreferencesHelper.getUsername();
	}
	
	@Override
	public CharSequence getEmail() {
		return mPreferencesHelper.getEmail();
	}
	
	@Override
	public Date getEntryDbDate() {
		return mPreferencesHelper.getEntryDbDate();
	}
	
	@Override
	public Boolean getIsUsernameAutofillEnabled() {
		return mPreferencesHelper.getIsUsernameAutofillEnabled();
	}
	
	@Override
	public Boolean getIsAutoSyncEnabled() {
		return mPreferencesHelper.getIsAutoSyncEnabled();
	}
	
	@Override
	public CharSequence getPasswordHash() {
		return mPreferencesHelper.getPasswordHash();
	}
	
	@Override
	public CharSequence getServerAddress() {
		return mPreferencesHelper.getServerAddress();
	}
	
	@Override
	public void setUsername(CharSequence username) {
		mPreferencesHelper.setUsername(username);
	}
	
	@Override
	public void setEmail(CharSequence email) {
		mPreferencesHelper.setEmail(email);
	}
	
	@Override
	public void setEntryDbDate(Date entryDbDate) {
		mPreferencesHelper.setEntryDbDate(entryDbDate);
	}
	
	@Override
	public void setUserDetails(CharSequence username, CharSequence email, Date entryDbDate) {
		mPreferencesHelper.setUserDetails(username, email, entryDbDate);
	}
	
	@Override
	public void setPasswordHash(CharSequence hash) {
		mPreferencesHelper.setPasswordHash(hash);
	}
	
	@Override
	public void setServerAddress(CharSequence address) {
		mPreferencesHelper.setServerAddress(address);
		mApiHelper.setCurrentServerAddress(address);
	}
	
	@Override
	public void setHeaderDetails(CharSequence username, CharSequence password) {
		mApiHelper.setHeaderDetails(username, password);
	}
	
	@Override
	public void setCurrentServerAddress(CharSequence address) {
		mApiHelper.setCurrentServerAddress(address);
	}
	
	@Override
	public CharSequence getPassword() {
		return mApiHelper.getPassword();
	}
	
	@Override
	public Single<UserDetailsResponse> doUserDetailsApiCall() {
		return mApiHelper.doUserDetailsApiCall();
	}
	
	@Override
	public Single<UserValidationResponse> doUserRegistrationApiCall(UserRequest request) {
		return mApiHelper.doUserRegistrationApiCall(request);
	}
	
	@Override
	public Single<UserValidationResponse> doEditEmailApiCall(UserRequest request) {
		return mApiHelper.doEditEmailApiCall(request);
	}
	
	@Override
	public Single<UserValidationResponse> doEditPasswordApiCall(UserRequest request) {
		return mApiHelper.doEditPasswordApiCall(request);
	}
	
	@Override
	public Single<EntryDatabase> doDownloadEntryDbApiCall() {
		return mApiHelper.doDownloadEntryDbApiCall();
	}
	
	@Override
	public Single<UserValidationResponse> doUploadEntryDbApiCall(EntryDatabase entryDatabase) {
		return mApiHelper.doUploadEntryDbApiCall(entryDatabase);
	}
	
	@Override
	public Date getLastSyncDate() {
		return lastSync;
	}
	
	@Override
	public void setLastSyncDate(Date date) {
		lastSync = date;
	}
	
	@Override
	public Completable clearAllTables() {
		return mDbHelper.clearAllTables();
	}
	
	@Override
	public Boolean isPasswordValid(CharSequence password) {
		if (mPreferencesHelper.getPasswordHash().toString().equals("")) {
			return false;
		}
		return BCrypt.checkpw(password.toString(), mPreferencesHelper.getPasswordHash().toString());
	}
	
	@Override
	public boolean getOfflineMode() {
		return mOfflineMode;
	}
	
	@Override
	public void setOfflineMode(boolean isOffline) {
		mOfflineMode = isOffline;
	}
	
}
