package hu.elte.passchest.data;

import hu.elte.passchest.data.local.db.DbHelper;
import hu.elte.passchest.data.local.prefs.PreferencesHelper;
import hu.elte.passchest.data.remote.ApiHelper;
import io.reactivex.Completable;

import java.util.Date;

public interface DataManager extends DbHelper, PreferencesHelper, ApiHelper {
	
	Date getLastSyncDate();
	
	void setLastSyncDate(Date date);
	
	Completable clearAllTables();
	
	Boolean isPasswordValid(CharSequence password);
	
	boolean getOfflineMode();
	
	void setOfflineMode(boolean isOffline);
	
}
