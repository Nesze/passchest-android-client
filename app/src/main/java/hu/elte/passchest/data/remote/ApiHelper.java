package hu.elte.passchest.data.remote;

import hu.elte.passchest.data.model.api.entrydatabase.EntryDatabase;
import hu.elte.passchest.data.model.api.user.UserDetailsResponse;
import hu.elte.passchest.data.model.api.user.UserRequest;
import hu.elte.passchest.data.model.api.user.UserValidationResponse;
import io.reactivex.Single;

public interface ApiHelper {
	
	void setHeaderDetails(CharSequence username, CharSequence password);
	
	void setCurrentServerAddress(CharSequence address);
	
	CharSequence getPassword();

	Single<UserDetailsResponse> doUserDetailsApiCall();
	
	Single<UserValidationResponse> doUserRegistrationApiCall(UserRequest request);
	
	Single<UserValidationResponse> doEditEmailApiCall(UserRequest request);
	
	Single<UserValidationResponse> doEditPasswordApiCall(UserRequest request);
	
	Single<EntryDatabase> doDownloadEntryDbApiCall();
	
	Single<UserValidationResponse> doUploadEntryDbApiCall(EntryDatabase entryDatabase);

}
