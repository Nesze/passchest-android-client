package hu.elte.passchest.data.model.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import hu.elte.passchest.data.model.api.entrydatabase.Status;

import java.util.Date;
import java.util.UUID;

@Entity(tableName = "categories")
public class Category {

	@PrimaryKey
	@NonNull
	@ColumnInfo(name = "uuid")
	public UUID uuid;
	
	@ColumnInfo(name = "status")
	public Status status;
	
	@ColumnInfo(name = "name")
	public String name;
	
	@ColumnInfo(name = "date_modified")
	public Date dateModified;

}
