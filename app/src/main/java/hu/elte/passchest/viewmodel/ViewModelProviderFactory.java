package hu.elte.passchest.viewmodel;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import hu.elte.passchest.data.DataManager;
import hu.elte.passchest.ui.addresseditor.AddressEditorViewModel;
import hu.elte.passchest.ui.auth.AuthViewModel;
import hu.elte.passchest.ui.categoryeditor.CategoryEditorViewModel;
import hu.elte.passchest.ui.categoryview.CategoryViewViewModel;
import hu.elte.passchest.ui.entryeditor.EntryEditorViewModel;
import hu.elte.passchest.ui.entryview.EntryViewViewModel;
import hu.elte.passchest.ui.main.MainViewModel;
import hu.elte.passchest.ui.registration.RegistrationViewModel;
import hu.elte.passchest.ui.settings.editemail.EditEmailViewModel;
import hu.elte.passchest.ui.settings.editpassword.EditPasswordViewModel;
import hu.elte.passchest.utils.rx.SchedulerProvider;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ViewModelProviderFactory extends ViewModelProvider.NewInstanceFactory {

    private final DataManager mDataManager;
    
    private final SchedulerProvider mSchedulerProvider;

    @Inject
    public ViewModelProviderFactory(DataManager dataManager, SchedulerProvider schedulerProvider) {
        mDataManager = dataManager;
        mSchedulerProvider = schedulerProvider;
    }


    @NotNull
    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(AuthViewModel.class)) {
            //noinspection unchecked
            return (T) new AuthViewModel(mDataManager, mSchedulerProvider);
        } else if (modelClass.isAssignableFrom(AddressEditorViewModel.class)) {
            //noinspection unchecked
            return (T) new AddressEditorViewModel(mDataManager, mSchedulerProvider);
        } else if (modelClass.isAssignableFrom(RegistrationViewModel.class)) {
            //noinspection unchecked
            return (T) new RegistrationViewModel(mDataManager, mSchedulerProvider);
        } else if (modelClass.isAssignableFrom(MainViewModel.class)) {
            //noinspection unchecked
            return (T) new MainViewModel(mDataManager, mSchedulerProvider);
        } else if (modelClass.isAssignableFrom(EntryViewViewModel.class)) {
            //noinspection unchecked
            return (T) new EntryViewViewModel(mDataManager, mSchedulerProvider);
        } else if (modelClass.isAssignableFrom(CategoryViewViewModel.class)) {
            //noinspection unchecked
            return (T) new CategoryViewViewModel(mDataManager, mSchedulerProvider);
        } else if (modelClass.isAssignableFrom(EntryEditorViewModel.class)) {
            //noinspection unchecked
            return (T) new EntryEditorViewModel(mDataManager, mSchedulerProvider);
        } else if (modelClass.isAssignableFrom(CategoryEditorViewModel.class)) {
            //noinspection unchecked
            return (T) new CategoryEditorViewModel(mDataManager, mSchedulerProvider);
        } else if (modelClass.isAssignableFrom(EditEmailViewModel.class)) {
            //noinspection unchecked
            return (T) new EditEmailViewModel(mDataManager, mSchedulerProvider);
        } else if (modelClass.isAssignableFrom(EditPasswordViewModel.class)) {
            //noinspection unchecked
            return (T) new EditPasswordViewModel(mDataManager, mSchedulerProvider);
        }
        throw new IllegalArgumentException("Unknown ViewModel class: " + modelClass.getName());
    }
}
